package grpcserver

import (
	"context"
	"path"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type InfoExtended struct {
	Service   string
	Method    string
	RequestID string
}

func makeInfoExtended(ctx context.Context, info *grpc.UnaryServerInfo) InfoExtended {
	var infoExtended InfoExtended
	infoExtended.Service = path.Dir(info.FullMethod)[1:]
	infoExtended.Method = path.Base(info.FullMethod)

	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		infoExtended.RequestID = headerValue(md, "X-Request-Id")
	}

	return infoExtended
}

func headerValue(md metadata.MD, headerName string) string {
	values := md.Get(headerName)
	if len(values) > 0 {
		return values[0]
	}
	return ""
}
