// Package grpcserver is a wrapper for gRPC server
package grpcserver

import (
	"context"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/reflection"
)

type Server struct {
	lis          net.Listener
	server       *grpc.Server
	healthServer *health.Server

	useReflection bool
	served        chan struct{}
}

func NewServer(lis net.Listener, opt ...grpc.ServerOption) *Server {
	s := &Server{
		lis:          lis,
		server:       grpc.NewServer(opt...),
		healthServer: health.NewServer(),
		served:       make(chan struct{}),
	}
	s.server.RegisterService(&healthpb.Health_ServiceDesc, s.healthServer)
	s.SetServingStatus("", healthpb.HealthCheckResponse_SERVING)
	return s
}

func (s *Server) SetServingStatus(service string, status healthpb.HealthCheckResponse_ServingStatus) {
	s.healthServer.SetServingStatus(service, status)
}

func (s *Server) RegisterService(sd *grpc.ServiceDesc, ss any) {
	if s.isServed() {
		panic("grpcserver: the server has already been started")
	}
	s.server.RegisterService(sd, ss)
	s.SetServingStatus(sd.ServiceName, healthpb.HealthCheckResponse_SERVING)
}

func (s *Server) RegisterReflection() {
	if s.isServed() {
		panic("grpcserver: the server has already been started")
	}
	s.useReflection = true
}

func (s *Server) ListenAndServe(ctx context.Context) (err error) {
	errc := make(chan error, 1)
	go func() { errc <- s.Serve() }()

	select {
	case <-ctx.Done():
	case err = <-errc:
	}

	s.Shutdown()

	return err
}

func (s *Server) Serve() (err error) {
	if s.useReflection {
		reflection.Register(s.server)
	}

	close(s.served)

	return s.server.Serve(s.lis)
}

func (s *Server) isServed() bool {
	select {
	case <-s.served:
		return true
	default:
		return false
	}
}

func (s *Server) Shutdown() {
	s.healthServer.Shutdown()
	s.server.GracefulStop()
}

func (s *Server) Stop() {
	s.healthServer.Shutdown()
	s.server.Stop()
}
