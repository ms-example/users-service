package grpcserver

import (
	"context"
	"fmt"
	"runtime"

	"github.com/rs/zerolog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func Recovery(retCode codes.Code, logger *zerolog.Logger) grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req any,
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (_ any, err error) {
		defer func() {
			if r := recover(); r != nil {
				err = status.Error(retCode, "unexpected error")

				infoExtended := makeInfoExtended(ctx, info)
				logger.Error().
					Err(fmt.Errorf("%v", r)).
					Str("scope", "grpc").
					Str("grpc_service", infoExtended.Service).
					Str("grpc_method", infoExtended.Method).
					Str("request_id", infoExtended.RequestID).
					Str("stack_trace", stackTrace()).
					Bool("alert", true).
					Msg("panic")
			}
		}()

		return handler(ctx, req)
	}
}

func stackTrace() string {
	stack := make([]byte, 64<<10)
	stack = stack[:runtime.Stack(stack, false)]
	return string(stack)
}
