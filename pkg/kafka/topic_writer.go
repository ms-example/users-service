// Package kafka provides some functionality for working with Kafka
package kafka

import (
	"io"

	"github.com/Shopify/sarama"
	"github.com/rs/zerolog"
)

type topicWriter struct {
	producer    sarama.AsyncProducer
	topic       string
	trimNewLine bool
	logger      *zerolog.Logger
}

func NewTopicWriter(
	producer sarama.AsyncProducer,
	topic string,
	trimNewLine bool,
	logger ...*zerolog.Logger,
) io.Writer {
	w := &topicWriter{
		producer:    producer,
		topic:       topic,
		trimNewLine: trimNewLine,
	}

	if len(logger) > 0 {
		l := logger[0].With().
			Str("scope", "kafka_topic_writer").
			Str("topic", topic).
			Logger()
		w.logger = &l
	}

	go w.dispatch()

	return w
}

func (w *topicWriter) dispatch() {
	for {
		select {
		case _, ok := <-w.producer.Successes():
			if !ok {
				return
			}
		case err, ok := <-w.producer.Errors():
			if !ok {
				return
			}
			if w.logger != nil {
				w.logger.Error().Err(err).Msg("produce message error")
			}
		}
	}
}

func (w *topicWriter) Write(p []byte) (int, error) {
	msg := &sarama.ProducerMessage{
		Topic: w.topic,
		Value: w.makeValue(p),
	}

	w.producer.Input() <- msg

	return len(p), nil
}

func (w *topicWriter) makeValue(p []byte) sarama.ByteEncoder {
	var value sarama.ByteEncoder

	if len(p) == 0 {
		return value
	}

	if w.trimNewLine && p[len(p)-1] == '\n' {
		value = make(sarama.ByteEncoder, len(p)-1)
		copy(value, p[:len(p)-1])
	} else {
		value = make(sarama.ByteEncoder, len(p))
		copy(value, p)
	}

	return value
}
