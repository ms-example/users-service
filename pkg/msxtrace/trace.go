// Package msxtrace provides Trace function and tracer for pgx package
package msxtrace

import (
	"context"
	"runtime"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

// Trace starts new trace span
//
//nolint:ireturn,nolintlint // it's ok here
func Trace(
	ctx context.Context,
	spanName string,
	opts ...trace.SpanStartOption,
) (context.Context, trace.Span, func(errs ...error)) {
	if spanName == "" {
		spanName = makeSpanName()
	}

	tracer := otel.GetTracerProvider().Tracer("")
	ctx, span := tracer.Start(ctx, spanName, opts...)

	finish := func(errs ...error) {
		if len(errs) > 0 {
			err := errs[0]
			if err != nil {
				span.SetStatus(codes.Error, err.Error())
			}
		}

		span.End()
	}

	return ctx, span, finish
}

func makeSpanName() string {
	pc, spanName, _, ok := runtime.Caller(2)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		spanName = details.Name()
	}
	return spanName
}
