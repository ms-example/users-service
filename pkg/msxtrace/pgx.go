package msxtrace

import (
	"context"
	"time"

	"github.com/jackc/pgx/v5"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

type ctxKey int

const (
	_ ctxKey = iota
	msxQueryCtxKey
)

type traceQueryData struct {
	startTime time.Time
	span      trace.Span
	finish    func(err ...error)
}

type PGXTracer struct{}

func (*PGXTracer) TraceQueryStart(ctx context.Context, _ *pgx.Conn, data pgx.TraceQueryStartData) context.Context {
	ctx, span, finish := Trace(ctx, "pgx")
	span.SetAttributes(
		attribute.String("query", data.SQL),
	)
	return context.WithValue(ctx, msxQueryCtxKey, traceQueryData{
		startTime: time.Now(),
		span:      span,
		finish:    finish,
	})
}

func (*PGXTracer) TraceQueryEnd(ctx context.Context, _ *pgx.Conn, data pgx.TraceQueryEndData) {
	queryData, ok := ctx.Value(msxQueryCtxKey).(traceQueryData)
	if !ok {
		return
	}

	elapsed := time.Since(queryData.startTime)
	queryData.span.SetAttributes(
		attribute.String("elapsed", elapsed.String()),
	)
	queryData.finish(data.Err)
}
