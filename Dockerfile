# syntax=docker/dockerfile:experimental

FROM golang:1.20 AS build
#ENV GOPRIVATE gitlab.com/ms-example
ENV CGO_ENABLED 0

ARG NAME
ARG CI_JOB_TOKEN

WORKDIR /go/src/service

COPY . .

#RUN git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@${GOPRIVATE}/".insteadOf https://${GOPRIVATE}/

RUN --mount=type=cache,target=/go/pkg/mod \
    --mount=type=cache,target=/root/.cache/go-build \
    go mod download && \
    go build --ldflags "-w -s -extldflags -static -X 'main.commit=$(git rev-parse HEAD)' -X 'main.branch=$(git branch --show-current)'" -o ./bin/application ./cmd/$NAME/


FROM alpine:latest
WORKDIR /app
COPY --from=build /go/src/service/bin/application ./application
COPY --from=build /go/src/service/deployments ./deployments
COPY --from=build /go/src/service/templates ./templates
RUN chown nobody: /app -R

# REST
EXPOSE 8080
# TECH
EXPOSE 8081
# gRPC
EXPOSE 8090
EXPOSE 8091

USER nobody
CMD ["/app/application"]
