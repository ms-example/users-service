package domain

import (
	"errors"
)

var (
	ErrEmailAlreadyInUse = errors.New("email already in use")
	ErrInvalidPassword   = errors.New("invalid password")
	ErrInvalidToken      = errors.New("invalid token")
)
