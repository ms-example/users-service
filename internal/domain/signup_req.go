package domain

type SignUpRequest struct {
	Name     string
	LastName string
	Email    string
	Password string
}
