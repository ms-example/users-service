package domain

import (
	"github.com/golang-jwt/jwt"
)

type TokenType string

const (
	TokenTypeAccess        TokenType = "accessToken"
	TokenTypeRefresh       TokenType = "refreshToken"
	TokenTypeResetPassword TokenType = "resetPassword"
)

type TokenClaims struct {
	jwt.StandardClaims
	TokenType TokenType `json:"type"`
	UserUUID  string    `json:"userUuid"`
}
