// Package domain contains types of domain area entities
package domain

import (
	"time"
)

type UserID int

func (id UserID) Int() int {
	return int(id)
}

type User struct {
	ID           UserID
	UUID         string
	FirstName    string
	LastName     string
	Email        string
	PasswordHash string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	LastLoginAt  time.Time
}
