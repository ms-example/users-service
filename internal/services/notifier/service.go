// Package notifier is a gRPC client for notifier services
package notifier

import (
	"context"
	"fmt"

	"gitlab.com/ms-example/users-service/internal/contract"

	"gitlab.com/ms-example/protobuf/notifierpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var _ contract.Notifier = (*Service)(nil)

type Service struct {
	client notifierpb.NotifierServiceClient
}

func New(conn grpc.ClientConnInterface) *Service {
	return &Service{
		client: notifierpb.NewNotifierServiceClient(conn),
	}
}

func (s *Service) SendEmail(ctx context.Context, req *contract.SendEmailRequest) error {
	grpcReq := notifierpb.SendEmailRequest{
		Email:   req.Email,
		Subject: req.Subject,
		Body:    req.Body,
	}

	_, err := s.client.SendEmail(ctx, &grpcReq)
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			return err
		}

		switch st.Code() {
		case codes.InvalidArgument:
			return fmt.Errorf("%w: %v", contract.ErrInvalidRequest, st.Message())
		default:
			return err
		}
	}

	return nil
}
