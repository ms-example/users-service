// Package tokenizer creates and verifies JWT tokens
package tokenizer

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/ms-example/users-service/internal/domain"

	"github.com/golang-jwt/jwt"
)

var (
	errUnexpectedMethod = errors.New("unexpected token signing method")
	errInvalidType      = errors.New("invalid token type")
)

type Service struct {
	secretKey        []byte
	accessTTL        time.Duration
	refreshTTL       time.Duration
	resetPasswordTTL time.Duration
}

func New(secretKey []byte, accessTTL, refreshTTL, resetPasswordTTL time.Duration) *Service {
	return &Service{
		secretKey:        secretKey,
		accessTTL:        accessTTL,
		refreshTTL:       refreshTTL,
		resetPasswordTTL: resetPasswordTTL,
	}
}

func (s *Service) AccessToken(user *domain.User) (string, error) {
	return s.generateToken(user, domain.TokenTypeAccess, s.accessTTL)
}

func (s *Service) RefreshToken(user *domain.User) (string, error) {
	return s.generateToken(user, domain.TokenTypeRefresh, s.refreshTTL)
}

func (s *Service) ResetPasswordToken(user *domain.User) (string, error) {
	return s.generateToken(user, domain.TokenTypeResetPassword, s.resetPasswordTTL)
}

func (s *Service) VerifyToken(token string, tokenType domain.TokenType) (*domain.TokenClaims, error) {
	claims, err := s.verifyToken(token, tokenType)
	if err != nil {
		return nil, fmt.Errorf("%w: %v", domain.ErrInvalidToken, err)
	}
	return claims, nil
}

func (s *Service) verifyToken(token string, tokenType domain.TokenType) (*domain.TokenClaims, error) {
	parsed, err := jwt.ParseWithClaims(
		token,
		&domain.TokenClaims{},
		func(token *jwt.Token) (any, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, errUnexpectedMethod
			}
			return s.secretKey, nil
		},
	)
	if err != nil {
		return nil, err
	}

	claims := parsed.Claims.(*domain.TokenClaims)
	if claims.TokenType != tokenType {
		return nil, errInvalidType
	}

	return claims, nil
}

func (s *Service) generateToken(user *domain.User, tokenType domain.TokenType, ttl time.Duration) (string, error) {
	now := time.Now().UTC()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &domain.TokenClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: now.Add(ttl).Unix(),
			IssuedAt:  now.Unix(),
		},
		TokenType: tokenType,
		UserUUID:  user.UUID,
	})
	return token.SignedString(s.secretKey)
}
