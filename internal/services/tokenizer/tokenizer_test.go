package tokenizer

import (
	"testing"
	"time"

	"gitlab.com/ms-example/users-service/internal/domain"

	"github.com/golang-jwt/jwt"
	"github.com/golang-jwt/jwt/test"
	"github.com/stretchr/testify/require"
)

func TestService_AccessToken(t *testing.T) {
	srv := New([]byte("secret"), time.Minute, time.Minute, time.Minute)
	user := domain.User{
		UUID: "some_uuid",
	}
	token, err := srv.AccessToken(&user)
	require.Nil(t, err)
	require.NotEmpty(t, token)
}

func TestService_RefreshToken(t *testing.T) {
	srv := New([]byte("secret"), time.Minute, time.Minute, time.Minute)
	user := domain.User{
		UUID: "some_uuid",
	}
	token, err := srv.RefreshToken(&user)
	require.Nil(t, err)
	require.NotEmpty(t, token)
}

func TestService_ResetPasswordToken(t *testing.T) {
	srv := New([]byte("secret"), time.Minute, time.Minute, time.Minute)
	user := domain.User{
		UUID: "some_uuid",
	}
	token, err := srv.ResetPasswordToken(&user)
	require.Nil(t, err)
	require.NotEmpty(t, token)
}

func TestService_VerifyToken(t *testing.T) {
	srv := New([]byte("secret"), time.Second, time.Second, time.Minute)
	user := domain.User{
		UUID: "some_uuid",
	}

	accessToken, err := srv.AccessToken(&user)
	require.Nil(t, err)

	refreshToken, err := srv.RefreshToken(&user)
	require.Nil(t, err)

	t.Run("valid access token", func(t *testing.T) {
		claims, err := srv.VerifyToken(accessToken, domain.TokenTypeAccess)
		require.Nil(t, err)
		require.Equal(t, user.UUID, claims.UserUUID)
		require.Equal(t, domain.TokenTypeAccess, claims.TokenType)
	})

	t.Run("valid refresh token", func(t *testing.T) {
		claims, err := srv.VerifyToken(refreshToken, domain.TokenTypeRefresh)
		require.Nil(t, err)
		require.Equal(t, user.UUID, claims.UserUUID)
		require.Equal(t, domain.TokenTypeRefresh, claims.TokenType)
	})

	t.Run("valid reset password token", func(t *testing.T) {
		resetPasswordToken, err := srv.ResetPasswordToken(&user)
		require.Nil(t, err)

		claims, err := srv.VerifyToken(resetPasswordToken, domain.TokenTypeResetPassword)
		require.Nil(t, err)
		require.Equal(t, user.UUID, claims.UserUUID)
		require.Equal(t, domain.TokenTypeResetPassword, claims.TokenType)
	})

	t.Run("wrong token type", func(t *testing.T) {
		claims, err := srv.VerifyToken(refreshToken, domain.TokenTypeAccess)
		require.Error(t, err)
		require.ErrorIs(t, err, domain.ErrInvalidToken)
		require.Nil(t, claims)
	})

	t.Run("expired access token", func(t *testing.T) {
		time.Sleep(time.Second * 2)
		claims, err := srv.VerifyToken(accessToken, domain.TokenTypeAccess)
		require.Error(t, err)
		require.ErrorIs(t, err, domain.ErrInvalidToken)
		require.Nil(t, claims)
	})

	t.Run("wrong signing method", func(t *testing.T) {
		now := time.Now()
		token, err := jwt.NewWithClaims(jwt.SigningMethodPS256, &domain.TokenClaims{
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: now.Add(time.Minute).Unix(),
				IssuedAt:  now.Unix(),
			},
			TokenType: domain.TokenTypeAccess,
			UserUUID:  user.UUID,
		}).SignedString(test.LoadRSAPrivateKeyFromDisk("test/sample_key"))
		require.Nil(t, err)
		require.NotEmpty(t, token)

		claims, err := srv.VerifyToken(token, domain.TokenTypeAccess)
		require.Error(t, err)
		require.ErrorIs(t, err, domain.ErrInvalidToken)
		require.Nil(t, claims)
	})
}
