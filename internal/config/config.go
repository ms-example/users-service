// Package config provides parsing configuration from environment variables
package config

import (
	"fmt"
	"net"
	"net/url"
	"strconv"
	"time"

	"github.com/caarlos0/env/v8"
	"github.com/rs/zerolog"
)

const (
	ResetPasswordEmailTemplateName = "reset_password"

	resetPwdPath = "/user/reset-password"
)

type Config struct {
	LogLevel         zerolog.Level `env:"LOG_LEVEL" envDefault:"info"`
	TechPort         string        `env:"TECH_PORT" envDefault:"8081"`
	GRPCPort         string        `env:"GRPC_PORT" envDefault:"8090"`
	GRPCPortInternal string        `env:"GRPC_PORT_INTERNAL" envDefault:"8091"`
	TemplatesPath    string        `env:"TEMPLATES_PATH" envDefault:"templates/*.gohtml"`
	FrontDomainName  string        `env:"FRONT_DOMAIN_NAME" envDefault:"front.localhost"`
	NotifierGRPCAddr string        `env:"NOTIFIER_GRPC_ADDR" envDefault:"notifier.localhost:8090"`
	Postgres         Postgres
	Kafka            Kafka
	Auth             Auth
	Jaeger           Jaeger
}

type Postgres struct {
	Host            string        `env:"POSTGRES_HOST" envDefault:"postgres"`
	Port            uint16        `env:"POSTGRES_PORT" envDefault:"5432"`
	User            string        `env:"POSTGRES_USER" envDefault:"user"`
	Password        string        `env:"POSTGRES_PASSWORD" envDefault:"password"`
	Database        string        `env:"POSTGRES_DATABASE_USERS" envDefault:"users"`
	MigrationsPath  string        `env:"POSTGRES_MIGRATIONS_PATH" envDefault:"deployments/migrations/postgres"`
	PoolSize        int32         `env:"POSTGRES_POOL_SIZE" envDefault:"3"`
	MaxConnLifetime time.Duration `env:"POSTGRES_MAX_CONN_LIFETIME" envDefault:"1m"`
	MaxConnIdleTime time.Duration `env:"POSTGRES_MAX_CONN_IDLE_TIME" envDefault:"30s"`
}

type Kafka struct {
	Addrs     []string `env:"KAFKA_BOOTSTRAP_SERVERS" envDefault:"kafka:9092"`
	LogsTopic string   `env:"KAFKA_LOGS_TOPIC" envDefault:"logs"`
}

type Auth struct {
	TokensSecret          string        `env:"AUTH_SECRET_KEY,required"`
	AccessTokenTTL        time.Duration `env:"ACCESS_TOKEN_TTL" envDefault:"1h"`
	RefreshTokenTTL       time.Duration `env:"REFRESH_TOKEN_TTL" envDefault:"3h"`
	ResetPasswordTokenTTL time.Duration `env:"RESET_PASSWORD_TOKEN_TTL" envDefault:"8h"`
}

type Jaeger struct {
	Endpoint string `env:"JAEGER_ENDPOINT" envDefault:"http://jaeger:14268/api/traces"`
}

func New() (*Config, error) {
	cfg := &Config{}

	if err := env.Parse(cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

func (c *Config) ResetPasswordURI() string {
	uri := url.URL{
		Scheme: "https",
		Host:   c.FrontDomainName,
		Path:   resetPwdPath,
	}
	return uri.String()
}

func (c *Postgres) Dial() string {
	var user *url.Userinfo

	if c.User != "" {
		var pass string

		if c.Password != "" {
			pass = c.Password
		}

		user = url.UserPassword(c.User, pass)
	}

	params := url.Values{}
	params.Set("sslmode", "disable")

	uri := url.URL{
		Scheme:   "postgresql",
		User:     user,
		Host:     net.JoinHostPort(c.Host, strconv.Itoa(int(c.Port))),
		Path:     c.Database,
		RawQuery: params.Encode(),
	}

	return uri.String()
}

func (c *Postgres) DialWithPoolSize() string {
	dial := c.Dial()
	return dial + fmt.Sprintf("&pool_max_conns=%d", c.PoolSize)
}
