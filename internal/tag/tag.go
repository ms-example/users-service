// Package tag contains tag names for logging and tracing
package tag

const (
	Scope       = "scope"
	Method      = "method"
	GRPCService = "grpc_service"
	GRPCMethod  = "grpc_method"
	UserID      = "user_id"
	UserUUID    = "user_uuid"
	Email       = "email"
	FirstName   = "first_name"
	LastName    = "last_name"
)
