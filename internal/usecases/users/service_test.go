package users

import (
	"context"
	"errors"
	"html/template"
	"testing"
	"time"

	"gitlab.com/ms-example/users-service/internal/contract"
	mocksContract "gitlab.com/ms-example/users-service/internal/contract/mocks"
	"gitlab.com/ms-example/users-service/internal/domain"
	repositoryPkg "gitlab.com/ms-example/users-service/internal/repository"
	"gitlab.com/ms-example/users-service/internal/usecases/users/mocks"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/bcrypt"
)

var errTest = errors.New("test error")

func TestService_SignUp(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		repo func() repository
	}
	type args struct {
		ctx context.Context
		req *domain.SignUpRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr error
	}{
		{
			name: "success",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						CheckEmailInUse(gomock.Any(), "user@example.com").
						Return(false, domain.UserID(0), nil)
					m.EXPECT().
						Create(gomock.Any(), gomock.Any()).
						Return(domain.UserID(1), nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &domain.SignUpRequest{
					Name:     "Name",
					LastName: "LastName",
					Email:    "user@example.com",
					Password: "qwertyasd",
				},
			},
		},
		{
			name: "email already in use",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						CheckEmailInUse(gomock.Any(), "user@example.com").
						Return(true, domain.UserID(1), nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &domain.SignUpRequest{
					Name:     "Name",
					LastName: "LastName",
					Email:    "user@example.com",
					Password: "qwertyasd",
				},
			},
			wantErr: domain.ErrEmailAlreadyInUse,
		},
		{
			name: "check email error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						CheckEmailInUse(gomock.Any(), "user@example.com").
						Return(false, domain.UserID(0), errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &domain.SignUpRequest{
					Name:     "Name",
					LastName: "LastName",
					Email:    "user@example.com",
					Password: "qwertyasd",
				},
			},
			wantErr: errTest,
		},
		{
			name: "create error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						CheckEmailInUse(gomock.Any(), "user@example.com").
						Return(false, domain.UserID(0), nil)
					m.EXPECT().
						Create(gomock.Any(), gomock.Any()).
						Return(domain.UserID(0), errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &domain.SignUpRequest{
					Name:     "Name",
					LastName: "LastName",
					Email:    "user@example.com",
					Password: "qwertyasd",
				},
			},
			wantErr: errTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.fields.repo(), nil, nil, nil, "")
			got, err := s.SignUp(tt.args.ctx, tt.args.req)
			if tt.wantErr != nil {
				require.Error(t, err)
				require.ErrorIs(t, err, tt.wantErr)
			} else {
				require.Nil(t, err)
				require.NotEmpty(t, got)
				parsed, err := uuid.Parse(got)
				require.Nil(t, err)
				require.NotEqual(t, uuid.Nil, parsed)
			}
		})
	}
}

func TestService_SignIn(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		repo          func() repository
		tokensService func() tokenizer
	}
	type args struct {
		ctx      context.Context
		email    string
		password string
	}
	tests := []struct {
		name             string
		fields           fields
		args             args
		wantAccessToken  string
		wantRefreshToken string
		wantErr          error
	}{
		{
			name: "success",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}, nil)
					m.EXPECT().
						UpdateLastLoginAt(gomock.Any(), domain.UserID(1)).
						Return(nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						AccessToken(&domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}).Return("access_token", nil)
					m.EXPECT().
						RefreshToken(&domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}).Return("refresh_token", nil)
					return m
				},
			},
			args: args{
				ctx:      context.Background(),
				email:    "user@example.com",
				password: "some_password",
			},
			wantAccessToken:  "access_token",
			wantRefreshToken: "refresh_token",
		},
		{
			name: "user not found by email",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{}, repositoryPkg.ErrNotFound)
					return m
				},
				tokensService: func() tokenizer { return nil },
			},
			args: args{
				ctx:      context.Background(),
				email:    "user@example.com",
				password: "some_password",
			},
			wantErr: repositoryPkg.ErrNotFound,
		},
		{
			name: "invalid password",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}, nil)
					return m
				},
				tokensService: func() tokenizer { return nil },
			},
			args: args{
				ctx:      context.Background(),
				email:    "user@example.com",
				password: "wrong_password",
			},
			wantErr: domain.ErrInvalidPassword,
		},
		{
			name: "create access token error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						AccessToken(&domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}).Return("", errTest)
					return m
				},
			},
			args: args{
				ctx:      context.Background(),
				email:    "user@example.com",
				password: "some_password",
			},
			wantErr: errTest,
		},
		{
			name: "create refresh token error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						AccessToken(&domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}).Return("access_token", nil)
					m.EXPECT().
						RefreshToken(&domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}).Return("", errTest)
					return m
				},
			},
			args: args{
				ctx:      context.Background(),
				email:    "user@example.com",
				password: "some_password",
			},
			wantErr: errTest,
		},
		{
			name: "update last login error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}, nil)
					m.EXPECT().
						UpdateLastLoginAt(gomock.Any(), domain.UserID(1)).
						Return(errTest)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						AccessToken(&domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}).Return("access_token", nil)
					m.EXPECT().
						RefreshToken(&domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "$2a$10$U7Mv.l4/.wq5ZCrDmGMW2.rYNVdfe75qFgxvay4i8gVjpSYvZ3Q72",
						}).Return("refresh_token", nil)
					return m
				},
			},
			args: args{
				ctx:      context.Background(),
				email:    "user@example.com",
				password: "some_password",
			},
			wantErr: errTest,
		},
		{
			name: "validate password error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:           1,
							UUID:         "4dd32cd2-49ec-496d-9dc4-284d12d3c3a2",
							Email:        "user@example.com",
							PasswordHash: "-",
						}, nil)
					return m
				},
				tokensService: func() tokenizer { return nil },
			},
			args: args{
				ctx:      context.Background(),
				email:    "user@example.com",
				password: "some_password",
			},
			wantErr: bcrypt.ErrHashTooShort,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.fields.repo(), tt.fields.tokensService(), nil, nil, "")
			gotAccessToken, gotRefreshToken, err := s.SignIn(tt.args.ctx, tt.args.email, tt.args.password)
			if tt.wantErr != nil {
				require.Error(t, err)
				require.ErrorIs(t, err, tt.wantErr)
			} else {
				require.Nil(t, err)
				require.Equal(t, tt.wantAccessToken, gotAccessToken)
				require.Equal(t, tt.wantRefreshToken, gotRefreshToken)
			}
		})
	}
}

func TestService_Refresh(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	user := domain.User{
		ID:        1,
		UUID:      uuid.NewString(),
		FirstName: "Name",
		LastName:  "LastName",
		Email:     "user@example.com",
	}
	refreshToken := "refresh_token"

	type fields struct {
		repo          func() repository
		tokensService func() tokenizer
	}
	type args struct {
		ctx          context.Context
		refreshToken string
	}
	tests := []struct {
		name                string
		fields              fields
		args                args
		wantAccessTokenNew  string
		wantRefreshTokenNew string
		wantErr             error
	}{
		{
			name: "success",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByUUID(gomock.Any(), user.UUID).
						Return(user, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken(refreshToken, domain.TokenTypeRefresh).
						Return(&domain.TokenClaims{UserUUID: user.UUID}, nil)
					m.EXPECT().
						AccessToken(&user).
						Return("new_access_token", nil)
					m.EXPECT().
						RefreshToken(&user).
						Return("new_refresh_token", nil)
					return m
				},
			},
			args: args{
				ctx:          context.Background(),
				refreshToken: refreshToken,
			},
			wantAccessTokenNew:  "new_access_token",
			wantRefreshTokenNew: "new_refresh_token",
		},
		{
			name: "invalid token",
			fields: fields{
				repo: func() repository { return mocks.NewMockrepository(ctrl) },
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken("invalid", domain.TokenTypeRefresh).
						Return(nil, domain.ErrInvalidToken)
					return m
				},
			},
			args: args{
				ctx:          context.Background(),
				refreshToken: "invalid",
			},
			wantErr: domain.ErrInvalidToken,
		},
		{
			name: "user not found",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByUUID(gomock.Any(), user.UUID).
						Return(domain.User{}, repositoryPkg.ErrNotFound)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken(refreshToken, domain.TokenTypeRefresh).
						Return(&domain.TokenClaims{UserUUID: user.UUID}, nil)
					return m
				},
			},
			args: args{
				ctx:          context.Background(),
				refreshToken: refreshToken,
			},
			wantErr: repositoryPkg.ErrNotFound,
		},
		{
			name: "generate access token error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByUUID(gomock.Any(), user.UUID).
						Return(user, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken(refreshToken, domain.TokenTypeRefresh).
						Return(&domain.TokenClaims{UserUUID: user.UUID}, nil)
					m.EXPECT().
						AccessToken(&user).
						Return("", errTest)
					return m
				},
			},
			args: args{
				ctx:          context.Background(),
				refreshToken: refreshToken,
			},
			wantErr: errTest,
		},
		{
			name: "generate refresh token error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByUUID(gomock.Any(), user.UUID).
						Return(user, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken(refreshToken, domain.TokenTypeRefresh).
						Return(&domain.TokenClaims{UserUUID: user.UUID}, nil)
					m.EXPECT().
						AccessToken(&user).
						Return("new_access_token", nil)
					m.EXPECT().
						RefreshToken(&user).
						Return("", errTest)
					return m
				},
			},
			args: args{
				ctx:          context.Background(),
				refreshToken: refreshToken,
			},
			wantErr: errTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.fields.repo(), tt.fields.tokensService(), nil, nil, "")
			gotAccessTokenNew, gotRefreshTokenNew, err := s.Refresh(tt.args.ctx, tt.args.refreshToken)
			if tt.wantErr != nil {
				require.Error(t, err)
				require.ErrorIs(t, err, tt.wantErr)
			} else {
				require.Nil(t, err)
				require.Equal(t, tt.wantAccessTokenNew, gotAccessTokenNew)
				require.Equal(t, tt.wantRefreshTokenNew, gotRefreshTokenNew)
			}
		})
	}
}

func TestService_Auth(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		repo          func() repository
		tokensService func() tokenizer
	}
	type args struct {
		ctx         context.Context
		accessToken string
		tokenType   domain.TokenType
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    domain.User
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByUUID(gomock.Any(), "uuid").
						Return(domain.User{
							ID:           1,
							UUID:         "uuid",
							FirstName:    "Name",
							LastName:     "LastName",
							Email:        "user@example.com",
							PasswordHash: "password_hash",
							CreatedAt:    time.Now().UTC().Truncate(time.Hour),
							UpdatedAt:    time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10),
							LastLoginAt:  time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15),
						}, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken("token", domain.TokenTypeAccess).
						Return(&domain.TokenClaims{UserUUID: "uuid"}, nil)
					return m
				},
			},
			args: args{
				ctx:         context.Background(),
				accessToken: "token",
				tokenType:   domain.TokenTypeAccess,
			},
			want: domain.User{
				ID:           1,
				UUID:         "uuid",
				FirstName:    "Name",
				LastName:     "LastName",
				Email:        "user@example.com",
				PasswordHash: "password_hash",
				CreatedAt:    time.Now().UTC().Truncate(time.Hour),
				UpdatedAt:    time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10),
				LastLoginAt:  time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15),
			},
		},
		{
			name: "invalid token",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken("token", domain.TokenTypeAccess).
						Return(nil, domain.ErrInvalidToken)
					return m
				},
			},
			args: args{
				ctx:         context.Background(),
				accessToken: "token",
				tokenType:   domain.TokenTypeAccess,
			},
			wantErr: true,
		},
		{
			name: "repo error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByUUID(gomock.Any(), "uuid").
						Return(domain.User{}, errTest)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						VerifyToken("token", domain.TokenTypeAccess).
						Return(&domain.TokenClaims{UserUUID: "uuid"}, nil)
					return m
				},
			},
			args: args{
				ctx:         context.Background(),
				accessToken: "token",
				tokenType:   domain.TokenTypeAccess,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.fields.repo(), tt.fields.tokensService(), nil, nil, "")
			got, err := s.Auth(tt.args.ctx, tt.args.accessToken, tt.args.tokenType)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.Nil(t, err)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func TestService_GetByID(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		repo func() repository
	}
	type args struct {
		ctx context.Context
		id  domain.UserID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    domain.User
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByID(gomock.Any(), domain.UserID(1)).
						Return(domain.User{
							ID:           1,
							UUID:         "124feb91-a289-48a6-befd-b04f5341a407",
							FirstName:    "Name",
							LastName:     "LastName",
							Email:        "user@example.com",
							PasswordHash: "password_hash",
							CreatedAt:    time.Now().UTC().Truncate(time.Hour),
							UpdatedAt:    time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10),
							LastLoginAt:  time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15),
						}, nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: domain.User{
				ID:           1,
				UUID:         "124feb91-a289-48a6-befd-b04f5341a407",
				FirstName:    "Name",
				LastName:     "LastName",
				Email:        "user@example.com",
				PasswordHash: "password_hash",
				CreatedAt:    time.Now().UTC().Truncate(time.Hour),
				UpdatedAt:    time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10),
				LastLoginAt:  time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15),
			},
		},
		{
			name: "error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByID(gomock.Any(), domain.UserID(1)).
						Return(domain.User{}, errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.fields.repo(), nil, nil, nil, "")
			got, err := s.GetByID(tt.args.ctx, tt.args.id)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.Nil(t, err)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func TestService_ForgotPassword(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	tmpl, _ := template.New("reset_password").Parse("{{ index . 0 }}|{{ index . 1 }}")
	tmplWrong, _ := template.New("reset_password").Parse("{{ index . 3 }}")

	type fields struct {
		repo                func() repository
		tokensService       func() tokenizer
		notifier            func() contract.Notifier
		resetPasswEmailTmpl *template.Template
		resetPasswordURI    string
	}
	type args struct {
		ctx   context.Context
		email string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						ResetPasswordToken(&domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}).
						Return("reset_password_token", nil)
					return m
				},
				notifier: func() contract.Notifier {
					m := mocksContract.NewMockNotifier(ctrl)
					m.EXPECT().
						SendEmail(gomock.Any(), &contract.SendEmailRequest{
							Email:   "user@example.com",
							Subject: resetPasswordSubject,
							Body:    "User|https://my-service/reset?token=reset_password_token",
						}).
						Return(nil)
					return m
				},
				resetPasswEmailTmpl: tmpl,
				resetPasswordURI:    "https://my-service/reset",
			},
			args: args{
				ctx:   context.Background(),
				email: "user@example.com",
			},
		},
		{
			name: "repo error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{}, errTest)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					return m
				},
				notifier: func() contract.Notifier {
					m := mocksContract.NewMockNotifier(ctrl)
					return m
				},
				resetPasswEmailTmpl: tmpl,
				resetPasswordURI:    "https://my-service/reset",
			},
			args: args{
				ctx:   context.Background(),
				email: "user@example.com",
			},
			wantErr: true,
		},
		{
			name: "tokenizer error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						ResetPasswordToken(&domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}).
						Return("", errTest)
					return m
				},
				notifier: func() contract.Notifier {
					m := mocksContract.NewMockNotifier(ctrl)
					return m
				},
				resetPasswEmailTmpl: tmpl,
				resetPasswordURI:    "https://my-service/reset",
			},
			args: args{
				ctx:   context.Background(),
				email: "user@example.com",
			},
			wantErr: true,
		},
		{
			name: "template error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						ResetPasswordToken(&domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}).
						Return("reset_password_token", nil)
					return m
				},
				notifier: func() contract.Notifier {
					m := mocksContract.NewMockNotifier(ctrl)
					return m
				},
				resetPasswEmailTmpl: tmplWrong,
				resetPasswordURI:    "https://my-service/reset",
			},
			args: args{
				ctx:   context.Background(),
				email: "user@example.com",
			},
			wantErr: true,
		},
		{
			name: "send email error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						GetByEmail(gomock.Any(), "user@example.com").
						Return(domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}, nil)
					return m
				},
				tokensService: func() tokenizer {
					m := mocks.NewMocktokenizer(ctrl)
					m.EXPECT().
						ResetPasswordToken(&domain.User{
							ID:        1,
							UUID:      "afff5719-f5ea-4501-8222-7d703bb6515d",
							FirstName: "User",
							LastName:  "Userovich",
							Email:     "user@example.com",
						}).
						Return("reset_password_token", nil)
					return m
				},
				notifier: func() contract.Notifier {
					m := mocksContract.NewMockNotifier(ctrl)
					m.EXPECT().
						SendEmail(gomock.Any(), &contract.SendEmailRequest{
							Email:   "user@example.com",
							Subject: resetPasswordSubject,
							Body:    "User|https://my-service/reset?token=reset_password_token",
						}).
						Return(errTest)
					return m
				},
				resetPasswEmailTmpl: tmpl,
				resetPasswordURI:    "https://my-service/reset",
			},
			args: args{
				ctx:   context.Background(),
				email: "user@example.com",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(
				tt.fields.repo(),
				tt.fields.tokensService(),
				tt.fields.notifier(),
				tt.fields.resetPasswEmailTmpl,
				tt.fields.resetPasswordURI,
			)
			err := s.ForgotPassword(tt.args.ctx, tt.args.email)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.Nil(t, err)
			}
		})
	}
}

func TestService_SetPassword(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		repo func() repository
	}
	type args struct {
		ctx      context.Context
		userID   domain.UserID
		password string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						SetPasswordHash(gomock.Any(), domain.UserID(1), gomock.Any()).
						Return(nil)
					return m
				},
			},
			args: args{
				ctx:      context.Background(),
				userID:   1,
				password: "new_password",
			},
		},
		{
			name: "error",
			fields: fields{
				repo: func() repository {
					m := mocks.NewMockrepository(ctrl)
					m.EXPECT().
						SetPasswordHash(gomock.Any(), domain.UserID(1), gomock.Any()).
						Return(errTest)
					return m
				},
			},
			args: args{
				ctx:      context.Background(),
				userID:   1,
				password: "new_password",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.fields.repo(), nil, nil, nil, "")
			err := s.SetPassword(tt.args.ctx, tt.args.userID, tt.args.password)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.Nil(t, err)
			}
		})
	}
}
