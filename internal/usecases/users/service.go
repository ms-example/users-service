// Package users provides users use cases
//
//go:generate mockgen -source=service.go -destination=./mocks/service.go -package mocks
package users

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"net/url"

	"gitlab.com/ms-example/users-service/internal/contract"
	"gitlab.com/ms-example/users-service/internal/domain"
	"gitlab.com/ms-example/users-service/internal/tag"
	"gitlab.com/ms-example/users-service/pkg/msxtrace"
	"gitlab.com/ms-example/users-service/pkg/passworder"

	"github.com/google/uuid"
	"go.opentelemetry.io/otel/attribute"
)

const (
	resetPasswordSubject = "Reset password instructions"
)

type (
	repository interface {
		Create(ctx context.Context, user *domain.User) (domain.UserID, error)
		CheckEmailInUse(ctx context.Context, email string) (inUse bool, id domain.UserID, err error)
		GetByID(ctx context.Context, id domain.UserID) (domain.User, error)
		GetByUUID(ctx context.Context, userUUID string) (domain.User, error)
		GetByEmail(ctx context.Context, email string) (domain.User, error)
		UpdateLastLoginAt(ctx context.Context, id domain.UserID) error
		SetPasswordHash(ctx context.Context, userID domain.UserID, passwordHash string) error
	}

	tokenizer interface {
		AccessToken(user *domain.User) (string, error)
		RefreshToken(user *domain.User) (string, error)
		VerifyToken(token string, tokenType domain.TokenType) (*domain.TokenClaims, error)
		ResetPasswordToken(user *domain.User) (string, error)
	}

	Service struct {
		repo                repository
		tokensService       tokenizer
		notifier            contract.Notifier
		resetPasswEmailTmpl *template.Template
		resetPasswordURI    string
	}
)

func New(
	repo repository,
	tokensService tokenizer,
	notifier contract.Notifier,
	resetPasswEmailTmpl *template.Template,
	resetPasswordURI string,
) *Service {
	return &Service{
		repo:                repo,
		tokensService:       tokensService,
		notifier:            notifier,
		resetPasswEmailTmpl: resetPasswEmailTmpl,
		resetPasswordURI:    resetPasswordURI,
	}
}

func (s *Service) SignUp(ctx context.Context, req *domain.SignUpRequest) (_ string, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(
		attribute.String(tag.Email, req.Email),
		attribute.String(tag.FirstName, req.Name),
		attribute.String(tag.LastName, req.LastName),
	)
	defer func() { finish(err) }()

	inUse, _, err := s.repo.CheckEmailInUse(ctx, req.Email)
	if err != nil {
		return "", fmt.Errorf("check email: %w", err)
	}

	if inUse {
		return "", domain.ErrEmailAlreadyInUse
	}

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    req.Name,
		LastName:     req.LastName,
		Email:        req.Email,
		PasswordHash: passworder.MustPasswordHash(req.Password),
	}

	_, err = s.repo.Create(ctx, &user)
	if err != nil {
		return "", fmt.Errorf("create user in db: %w", err)
	}

	span.SetAttributes(
		attribute.String(tag.UserUUID, user.UUID),
		attribute.Int(tag.UserID, user.ID.Int()),
	)

	return user.UUID, nil
}

func (s *Service) SignIn(ctx context.Context, email, password string) (accessToken, refreshToken string, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(
		attribute.String(tag.Email, email),
	)
	defer func() { finish(err) }()

	user, err := s.repo.GetByEmail(ctx, email)
	if err != nil {
		return "", "", fmt.Errorf("get user by email: %w", err)
	}

	isValid, err := passworder.ValidatePassword(password, user.PasswordHash)
	if err != nil {
		return "", "", fmt.Errorf("validate password: %w", err)
	}
	if !isValid {
		return "", "", domain.ErrInvalidPassword
	}

	accessToken, err = s.tokensService.AccessToken(&user)
	if err != nil {
		return "", "", fmt.Errorf("generate access token: %w", err)
	}

	refreshToken, err = s.tokensService.RefreshToken(&user)
	if err != nil {
		return "", "", fmt.Errorf("generate refresh token: %w", err)
	}

	err = s.repo.UpdateLastLoginAt(ctx, user.ID)
	if err != nil {
		return "", "", fmt.Errorf("update last login: %w", err)
	}

	return accessToken, refreshToken, nil
}

func (s *Service) Refresh(
	ctx context.Context,
	refreshToken string,
) (accessTokenNew, refreshTokenNew string, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	defer func() { finish(err) }()

	var claims *domain.TokenClaims
	claims, err = s.tokensService.VerifyToken(refreshToken, domain.TokenTypeRefresh)
	if err != nil {
		return "", "", fmt.Errorf("verify token: %w", err)
	}

	span.SetAttributes(attribute.String(tag.UserUUID, claims.UserUUID))

	var user domain.User
	user, err = s.repo.GetByUUID(ctx, claims.UserUUID)
	if err != nil {
		return "", "", fmt.Errorf("get user by UUID: %w", err)
	}

	accessTokenNew, err = s.tokensService.AccessToken(&user)
	if err != nil {
		return "", "", fmt.Errorf("generate access token: %w", err)
	}

	refreshTokenNew, err = s.tokensService.RefreshToken(&user)
	if err != nil {
		return "", "", fmt.Errorf("generate refresh token: %w", err)
	}

	return accessTokenNew, refreshTokenNew, nil
}

func (s *Service) Auth(ctx context.Context, accessToken string, tokenType domain.TokenType) (_ domain.User, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	defer func() { finish(err) }()

	claims, err := s.tokensService.VerifyToken(accessToken, tokenType)
	if err != nil {
		return domain.User{}, fmt.Errorf("verify token: %w", err)
	}

	user, err := s.repo.GetByUUID(ctx, claims.UserUUID)
	if err != nil {
		return domain.User{}, fmt.Errorf("get user by UUID: %w", err)
	}

	span.SetAttributes(
		attribute.Int(tag.UserID, user.ID.Int()),
		attribute.String(tag.UserUUID, user.UUID),
	)

	return user, nil
}

func (s *Service) GetByID(ctx context.Context, id domain.UserID) (_ domain.User, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.Int(tag.UserID, id.Int()))
	defer func() { finish(err) }()

	user, err := s.repo.GetByID(ctx, id)
	if err != nil {
		return domain.User{}, fmt.Errorf("get user by ID: %w", err)
	}

	return user, nil
}

func (s *Service) ForgotPassword(ctx context.Context, email string) (err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.String(tag.Email, email))
	defer func() { finish(err) }()

	user, err := s.repo.GetByEmail(ctx, email)
	if err != nil {
		return fmt.Errorf("get user by email: %w", err)
	}

	var token string
	token, err = s.tokensService.ResetPasswordToken(&user)
	if err != nil {
		return fmt.Errorf("generate reset password token: %w", err)
	}

	params := url.Values{}
	params.Set("token", token)

	linkURL := s.resetPasswordURI + "?" + params.Encode()

	var body bytes.Buffer
	err = s.resetPasswEmailTmpl.Execute(&body, []string{user.FirstName, linkURL})
	if err != nil {
		return fmt.Errorf("render email template: %w", err)
	}

	req := contract.SendEmailRequest{
		Email:   email,
		Subject: resetPasswordSubject,
		Body:    body.String(),
	}

	return s.notifier.SendEmail(ctx, &req)
}

func (s *Service) SetPassword(ctx context.Context, userID domain.UserID, password string) (err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.Int(tag.UserID, userID.Int()))
	defer func() { finish(err) }()

	passwordHash := passworder.MustPasswordHash(password)
	if err := s.repo.SetPasswordHash(ctx, userID, passwordHash); err != nil {
		return fmt.Errorf("set password hash: %w", err)
	}

	return nil
}
