package contract

import (
	"errors"
)

var ErrInvalidRequest = errors.New("invalid request")
