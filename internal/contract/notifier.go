// Package contract describes contracts for external services.
//
//go:generate mockgen -source=notifier.go -destination=./mocks/notifier.go -package mocks
package contract

import (
	"context"
)

type Notifier interface {
	SendEmail(ctx context.Context, req *SendEmailRequest) error
}

type SendEmailRequest struct {
	Email   string
	Subject string
	Body    string
}
