package users

import (
	"context"
	"errors"
)

var errNoTransaction = errors.New("transaction not started")

/*
	WithConsistency returns service with started new transaction.

Usage:

	repoTx, err := repo.WithConsistency(ctx)
	if err != nil {
		return err
	}
	defer repoTx.RollbackUnlessCommitted(ctx)
	// do something in transaction
	err = repoTx.Commit(ctx)
*/
func (s *Service) WithConsistency(ctx context.Context) (*Service, error) {
	tx, err := s.connPool.Begin(ctx)
	if err != nil {
		return nil, err
	}
	return &Service{connPool: s.connPool, tx: tx}, nil
}

func (s *Service) Commit(ctx context.Context) error {
	if s.tx == nil {
		return errNoTransaction
	}

	if err := s.tx.Commit(ctx); err != nil {
		return err
	}

	s.committed = true

	return nil
}

func (s *Service) RollbackUnlessCommitted(ctx context.Context) error {
	if s.tx == nil || s.committed {
		return nil
	}

	return s.tx.Rollback(ctx)
}
