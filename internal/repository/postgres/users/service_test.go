//go:build integration

package users

import (
	"context"
	"testing"
	"time"

	"gitlab.com/ms-example/users-service/internal/domain"
	"gitlab.com/ms-example/users-service/internal/repository"
	"gitlab.com/ms-example/users-service/internal/repository/postgres"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/stretchr/testify/require"
)

func TestService_Create(t *testing.T) {
	ctx := context.Background()
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya",
		LastName:     "Pupkin",
		Email:        "vasya@example.com",
		PasswordHash: "password_hash",
	}

	repo := New(conn)

	t.Run("success", func(t *testing.T) {
		userID, err := repo.Create(ctx, &user)
		require.Nil(t, err)
		require.NotZero(t, userID)
		require.Equal(t, userID, user.ID)
		require.NotZero(t, user.CreatedAt)
		require.NotZero(t, user.UpdatedAt)
		require.Zero(t, user.LastLoginAt)

		rows, err := conn.Query(ctx, `SELECT * FROM users WHERE id = $1 LIMIT 1`, userID.Int())
		require.Nil(t, err)
		defer rows.Close()

		userActual, err := pgx.CollectOneRow(rows, pgx.RowToStructByName[userModel])
		require.Nil(t, err)

		require.Equal(t, user, toDomain(&userActual))
	})

	t.Run("duplicate", func(t *testing.T) {
		_, err := repo.Create(ctx, &user)
		require.Error(t, err)
	})

	t.Run("error", func(t *testing.T) {
		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()
		_, err := repo.Create(ctxCanceled, &user)
		require.Error(t, err)
	})
}

func TestService_CheckEmailInUse(t *testing.T) {
	ctx := context.Background()
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya",
		LastName:     "Pupkin",
		Email:        "vasya@example.com",
		PasswordHash: "password_hash",
	}

	repo := New(conn)

	_, err := repo.Create(ctx, &user)
	require.Nil(t, err)

	t.Run("not found", func(t *testing.T) {
		inUse, id, err := repo.CheckEmailInUse(ctx, "nobody@example.com")
		require.Nil(t, err)
		require.Empty(t, id)
		require.False(t, inUse)
	})

	t.Run("found", func(t *testing.T) {
		inUse, id, err := repo.CheckEmailInUse(ctx, "vasya@example.com")
		require.Nil(t, err)
		require.Equal(t, user.ID, id)
		require.True(t, inUse)
	})

	t.Run("error", func(t *testing.T) {
		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()
		_, _, err := repo.CheckEmailInUse(ctxCanceled, "vasya@example.com")
		require.Error(t, err)
	})
}

func TestService_GetByID(t *testing.T) {
	ctx := context.Background()
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya",
		LastName:     "Pupkin",
		Email:        "vasya@example.com",
		PasswordHash: "password_hash",
	}

	repo := New(conn)

	_, err := repo.Create(ctx, &user)
	require.Nil(t, err)

	t.Run("found", func(t *testing.T) {
		userActual, err := repo.GetByID(ctx, user.ID)
		require.Nil(t, err)
		require.Equal(t, user, userActual)
	})

	t.Run("not found", func(t *testing.T) {
		_, err := repo.GetByID(ctx, 0)
		require.ErrorIs(t, err, repository.ErrNotFound)
	})

	t.Run("error", func(t *testing.T) {
		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()
		_, err := repo.GetByID(ctxCanceled, user.ID)
		require.Error(t, err)
	})
}

func TestService_GetByUUID(t *testing.T) {
	ctx := context.Background()
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya",
		LastName:     "Pupkin",
		Email:        "vasya@example.com",
		PasswordHash: "password_hash",
	}

	repo := New(conn)

	_, err := repo.Create(ctx, &user)
	require.Nil(t, err)

	t.Run("found", func(t *testing.T) {
		userActual, err := repo.GetByUUID(ctx, user.UUID)
		require.Nil(t, err)
		require.Equal(t, user, userActual)
	})

	t.Run("not found", func(t *testing.T) {
		_, err := repo.GetByUUID(ctx, uuid.NewString())
		require.ErrorIs(t, err, repository.ErrNotFound)
	})

	t.Run("error", func(t *testing.T) {
		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()
		_, err := repo.GetByUUID(ctxCanceled, user.UUID)
		require.Error(t, err)
	})
}

func TestService_GetByEmail(t *testing.T) {
	ctx := context.Background()
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya",
		LastName:     "Pupkin",
		Email:        "vasya@example.com",
		PasswordHash: "password_hash",
	}

	repo := New(conn)

	_, err := repo.Create(ctx, &user)
	require.Nil(t, err)

	t.Run("found", func(t *testing.T) {
		userActual, err := repo.GetByEmail(ctx, user.Email)
		require.Nil(t, err)
		require.Equal(t, user, userActual)
	})

	t.Run("not found", func(t *testing.T) {
		_, err := repo.GetByEmail(ctx, "nobody@example.com")
		require.ErrorIs(t, err, repository.ErrNotFound)
	})

	t.Run("error", func(t *testing.T) {
		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()
		_, err := repo.GetByEmail(ctxCanceled, user.Email)
		require.Error(t, err)
	})
}

func TestService_UpdateLastLoginAt(t *testing.T) {
	ctx := context.Background()
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	now := time.Now().UTC()

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya",
		LastName:     "Pupkin",
		Email:        "vasya@example.com",
		PasswordHash: "password_hash",
	}

	user2 := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya2",
		LastName:     "Pupkin2",
		Email:        "vasya2@example.com",
		PasswordHash: "password_hash",
	}

	repo := New(conn)

	_, err := repo.Create(ctx, &user)
	require.Nil(t, err)
	_, err = repo.Create(ctx, &user2)
	require.Nil(t, err)

	t.Run("success", func(t *testing.T) {
		err = repo.UpdateLastLoginAt(ctx, user.ID)
		require.Nil(t, err)

		// check user
		userActual, err := repo.GetByID(ctx, user.ID)
		require.Nil(t, err)
		require.GreaterOrEqual(t, userActual.LastLoginAt, now)
		require.Greater(t, userActual.UpdatedAt, now)

		// another users shouldn't be changed
		user2Actual, err := repo.GetByID(ctx, user2.ID)
		require.Nil(t, err)
		require.Equal(t, user2, user2Actual)
	})

	t.Run("error", func(t *testing.T) {
		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()
		err := repo.UpdateLastLoginAt(ctxCanceled, user.ID)
		require.Error(t, err)
	})
}

func TestService_SetPasswordHash(t *testing.T) {
	ctx := context.Background()
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	user := domain.User{
		UUID:         uuid.NewString(),
		FirstName:    "Vasya",
		LastName:     "Pupkin",
		Email:        "vasya@example.com",
		PasswordHash: "password_hash",
	}

	repo := New(conn)

	_, err := repo.Create(ctx, &user)
	require.Nil(t, err)

	t.Run("success", func(t *testing.T) {
		err := repo.SetPasswordHash(ctx, user.ID, "new_password_hash")
		require.Nil(t, err)

		userActual, err := repo.GetByID(ctx, user.ID)
		require.Nil(t, err)
		require.Equal(t, "new_password_hash", userActual.PasswordHash)
		require.Greater(t, userActual.UpdatedAt, user.UpdatedAt)
	})

	t.Run("error", func(t *testing.T) {
		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()

		err := repo.SetPasswordHash(ctxCanceled, user.ID, "new_password_hash")
		require.Error(t, err)
	})
}
