//go:build integration

package users

import (
	"context"
	"testing"

	"gitlab.com/ms-example/users-service/internal/repository/postgres"

	"github.com/stretchr/testify/require"
)

func TestService_WithConsistency(t *testing.T) {
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	s := New(conn)

	t.Run("success", func(t *testing.T) {
		sCons, err := s.WithConsistency(context.Background())
		require.Nil(t, err)
		t.Cleanup(func() {
			err := sCons.RollbackUnlessCommitted(context.Background())
			require.Nil(t, err)
		})
		require.NotNil(t, sCons.connPool)
		require.NotNil(t, sCons.tx)
		require.False(t, sCons.committed)
	})

	t.Run("error", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		sCons, err := s.WithConsistency(ctx)
		require.Error(t, err)
		require.Nil(t, sCons)
	})
}

func TestService_Commit(t *testing.T) {
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	s := New(conn)

	t.Run("success", func(t *testing.T) {
		sCons, err := s.WithConsistency(context.Background())
		require.Nil(t, err)
		err = sCons.Commit(context.Background())
		require.Nil(t, err)
		require.True(t, sCons.committed)
	})

	t.Run("no transaction", func(t *testing.T) {
		err := s.Commit(context.Background())
		require.Error(t, err)
		require.ErrorIs(t, err, errNoTransaction)
	})

	t.Run("commit error", func(t *testing.T) {
		sCons, err := s.WithConsistency(context.Background())
		require.Nil(t, err)

		ctxCanceled, cancel := context.WithCancel(context.Background())
		cancel()
		err = sCons.Commit(ctxCanceled)
		require.Error(t, err)
		require.False(t, sCons.committed)
	})
}

func TestService_RollbackUnlessCommitted(t *testing.T) {
	conn, teardown := postgres.SetupTesting(t, "users")
	t.Cleanup(teardown)

	s := New(conn)

	t.Run("success", func(t *testing.T) {
		sCons, err := s.WithConsistency(context.Background())
		require.Nil(t, err)
		err = sCons.RollbackUnlessCommitted(context.Background())
		require.Nil(t, err)
	})

	t.Run("error", func(t *testing.T) {
		sCons, err := s.WithConsistency(context.Background())
		require.Nil(t, err)
		ctxCancel, cancel := context.WithCancel(context.Background())
		cancel()
		err = sCons.RollbackUnlessCommitted(ctxCancel)
		require.Error(t, err)
	})

	t.Run("not consistency", func(t *testing.T) {
		err := s.RollbackUnlessCommitted(context.Background())
		require.Nil(t, err)
	})

	t.Run("already committed", func(t *testing.T) {
		sCons, err := s.WithConsistency(context.Background())
		require.Nil(t, err)
		err = sCons.Commit(context.Background())
		require.Nil(t, err)
		err = s.RollbackUnlessCommitted(context.Background())
		require.Nil(t, err)
	})
}
