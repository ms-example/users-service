// Package users is a users repository for PostgreSQL database
package users

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/ms-example/users-service/internal/domain"
	"gitlab.com/ms-example/users-service/internal/repository"
	"gitlab.com/ms-example/users-service/internal/tag"
	"gitlab.com/ms-example/users-service/pkg/msxtrace"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	"go.opentelemetry.io/otel/attribute"

	_ "github.com/lib/pq" // driver
)

type (
	session interface {
		Exec(ctx context.Context, sql string, arguments ...any) (commandTag pgconn.CommandTag, err error)
		Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error)
		QueryRow(ctx context.Context, sql string, args ...any) pgx.Row
	}

	Service struct {
		connPool  *pgxpool.Pool
		tx        pgx.Tx
		committed bool
	}
)

func New(connPool *pgxpool.Pool) *Service {
	return &Service{connPool: connPool}
}

func (s *Service) Create(ctx context.Context, user *domain.User) (_ domain.UserID, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(
		attribute.String(tag.Email, user.Email),
		attribute.String(tag.FirstName, user.FirstName),
		attribute.String(tag.LastName, user.LastName),
		attribute.String(tag.UserUUID, user.UUID),
	)
	defer func() { finish(err) }()

	// language=postgresql
	const query = `
INSERT INTO users (
	uuid,
	first_name,
	last_name,
	email,
	password_hash,
	created_at,
	updated_at
)
VALUES ($1, $2, $3, $4, $5, $6, $6)
RETURNING id;
`
	now := time.Now().UTC()
	user.CreatedAt = now
	user.UpdatedAt = now

	row := s.session().QueryRow(ctx, query,
		user.UUID,
		user.FirstName,
		user.LastName,
		user.Email,
		user.PasswordHash,
		now,
	)

	if err := row.Scan(&user.ID); err != nil {
		return 0, err
	}

	span.SetAttributes(attribute.Int(tag.UserID, user.ID.Int()))

	return user.ID, nil
}

func (s *Service) CheckEmailInUse(ctx context.Context, email string) (inUse bool, id domain.UserID, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.String(tag.Email, email))
	defer func() {
		if err == nil {
			span.SetAttributes(
				attribute.Bool("in_use", inUse),
				attribute.Int(tag.UserID, id.Int()),
			)
		}
		finish(err)
	}()

	// language=postgresql
	const query = `SELECT id FROM users WHERE email = $1`

	err = s.session().QueryRow(ctx, query, email).Scan(&id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return false, 0, nil
		}
		return false, 0, err
	}

	return true, id, nil
}

func (s *Service) GetByID(ctx context.Context, id domain.UserID) (_ domain.User, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.Int(tag.UserID, id.Int()))
	defer func() { finish(err) }()

	// language=postgresql
	const query = `SELECT * FROM users WHERE id = $1 LIMIT 1`

	rows, err := s.session().Query(ctx, query, id.Int())
	if err != nil {
		return domain.User{}, fmt.Errorf("query: %w", err)
	}

	defer rows.Close()

	user, err := pgx.CollectOneRow(rows, pgx.RowToStructByName[userModel])
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return domain.User{}, repository.ErrNotFound
		}
		return domain.User{}, fmt.Errorf("decode: %w", err)
	}

	span.SetAttributes(attribute.String(tag.UserUUID, user.UUID))

	return toDomain(&user), nil
}

func (s *Service) GetByUUID(ctx context.Context, uuid string) (_ domain.User, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.String(tag.UserUUID, uuid))
	defer func() { finish(err) }()

	// language=postgresql
	const query = `SELECT * FROM users WHERE uuid = $1 LIMIT 1`

	rows, err := s.session().Query(ctx, query, uuid)
	if err != nil {
		return domain.User{}, fmt.Errorf("query: %w", err)
	}

	defer rows.Close()

	user, err := pgx.CollectOneRow(rows, pgx.RowToStructByName[userModel])
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return domain.User{}, repository.ErrNotFound
		}
		return domain.User{}, fmt.Errorf("decode: %w", err)
	}

	span.SetAttributes(attribute.Int(tag.UserID, user.ID))

	return toDomain(&user), nil
}

func (s *Service) GetByEmail(ctx context.Context, email string) (_ domain.User, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.String(tag.Email, email))
	defer func() { finish(err) }()

	// language=postgresql
	const query = `SELECT * FROM users WHERE email = $1 LIMIT 1`

	rows, err := s.session().Query(ctx, query, email)
	if err != nil {
		return domain.User{}, fmt.Errorf("query: %w", err)
	}

	defer rows.Close()

	user, err := pgx.CollectOneRow(rows, pgx.RowToStructByName[userModel])
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return domain.User{}, repository.ErrNotFound
		}
		return domain.User{}, fmt.Errorf("decode: %w", err)
	}

	span.SetAttributes(attribute.Int(tag.UserID, user.ID))

	return toDomain(&user), nil
}

func (s *Service) UpdateLastLoginAt(ctx context.Context, id domain.UserID) (err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.Int(tag.UserID, id.Int()))
	defer func() { finish(err) }()

	// language=postgresql
	const query = `UPDATE users SET last_login_at = NOW(), updated_at = NOW() WHERE id = $1;`

	_, err = s.session().Exec(ctx, query, id)
	if err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

func (s *Service) SetPasswordHash(ctx context.Context, userID domain.UserID, passwordHash string) (err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.Int(tag.UserID, userID.Int()))
	defer func() { finish(err) }()

	// language=postgresql
	const query = `UPDATE users SET password_hash = $2, updated_at = NOW() WHERE id = $1;`

	_, err = s.session().Exec(ctx, query, userID.Int(), passwordHash)
	if err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

//nolint:ireturn,nolintlint // it's ok here
func (s *Service) session() session {
	if s.tx == nil {
		return s.connPool
	}

	return s.tx
}
