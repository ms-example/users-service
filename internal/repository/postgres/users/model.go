package users

import (
	"database/sql"
	"time"

	"gitlab.com/ms-example/users-service/internal/domain"
)

type userModel struct {
	ID           int          `db:"id"`
	UUID         string       `db:"uuid"`
	FirstName    string       `db:"first_name"`
	LastName     string       `db:"last_name"`
	Email        string       `db:"email"`
	PasswordHash string       `db:"password_hash"`
	CreatedAt    time.Time    `db:"created_at"`
	UpdatedAt    time.Time    `db:"updated_at"`
	LastLoginAt  sql.NullTime `db:"last_login_at"`
}

func toDomain(rec *userModel) domain.User {
	var lastLoginAt time.Time
	if rec.LastLoginAt.Valid {
		lastLoginAt = rec.LastLoginAt.Time.UTC()
	}
	return domain.User{
		ID:           domain.UserID(rec.ID),
		UUID:         rec.UUID,
		FirstName:    rec.FirstName,
		LastName:     rec.LastName,
		Email:        rec.Email,
		PasswordHash: rec.PasswordHash,
		CreatedAt:    rec.CreatedAt.UTC(),
		UpdatedAt:    rec.UpdatedAt.UTC(),
		LastLoginAt:  lastLoginAt,
	}
}
