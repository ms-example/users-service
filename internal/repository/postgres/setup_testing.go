// Package postgres contains repositories for PostgreSQL database
package postgres

import (
	"context"
	"testing"
	"time"

	"gitlab.com/ms-example/users-service/internal/config"
	"gitlab.com/ms-example/users-service/internal/migrator/postgres"

	"github.com/caarlos0/env/v8"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/rs/zerolog"
)

const migrationsPath = "../../../../deployments/migrations/postgres"

func SetupTesting(t *testing.T, tables ...string) (pool *pgxpool.Pool, teardown func()) {
	t.Helper()

	pgCfg := config.Postgres{}
	if err := env.Parse(&pgCfg); err != nil {
		t.Fatalf("parse postgres config: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	var err error
	pool, err = pgxpool.New(ctx, pgCfg.Dial())
	if err != nil {
		t.Fatalf("connect to postgres: %v", err)
	}

	if err := pool.Ping(ctx); err != nil {
		t.Fatalf("ping postgres: %v", err)
	}

	logger := zerolog.Nop()
	if err := postgres.Up(pgCfg.Dial(), migrationsPath, &logger); err != nil {
		t.Fatalf("up postgres migrations: %v", err)
	}

	for _, table := range tables {
		if _, err := pool.Exec(ctx, `TRUNCATE TABLE `+table); err != nil {
			t.Fatalf("truncate table %q: %v", table, err)
		}
	}

	return pool, func() { pool.Close() }
}
