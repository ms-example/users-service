// Package repository contains repositories for different databases
package repository

import (
	"errors"
)

var ErrNotFound = errors.New("not found")
