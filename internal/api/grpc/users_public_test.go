package grpc

import (
	"context"
	"errors"
	"testing"
	"time"

	"gitlab.com/ms-example/users-service/internal/api/grpc/mocks"
	"gitlab.com/ms-example/users-service/internal/domain"
	"gitlab.com/ms-example/users-service/internal/repository"
	"google.golang.org/protobuf/types/known/timestamppb"

	"github.com/golang/mock/gomock"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"gitlab.com/ms-example/protobuf/userspb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var errTest = errors.New("test error")

func TestUsersPublicService_SignUp(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		useCase func() usersUseCase
	}
	type args struct {
		ctx context.Context
		req *userspb.SignUpRequest
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		want        *userspb.SignUpResponse
		wantErr     bool
		wantErrCode codes.Code
	}{
		{
			name: "success",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						SignUp(gomock.Any(), &domain.SignUpRequest{
							Name:     "Name",
							LastName: "LastName",
							Email:    "user@example.com",
							Password: "QwertyAsd123",
						}).
						Return("c7d64634-cb9d-4b27-b6ce-440988e4ad89", nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "LastName",
					Password: "QwertyAsd123",
				},
			},
			want: &userspb.SignUpResponse{UserUuid: "c7d64634-cb9d-4b27-b6ce-440988e4ad89"},
		},
		{
			name:   "invalid request",
			fields: fields{useCase: func() usersUseCase { return nil }},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignUpRequest{},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "email already in use",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						SignUp(gomock.Any(), &domain.SignUpRequest{
							Name:     "Name",
							LastName: "LastName",
							Email:    "user@example.com",
							Password: "QwertyAsd123",
						}).
						Return("", domain.ErrEmailAlreadyInUse)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "LastName",
					Password: "QwertyAsd123",
				},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "error",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						SignUp(gomock.Any(), &domain.SignUpRequest{
							Name:     "Name",
							LastName: "LastName",
							Email:    "user@example.com",
							Password: "QwertyAsd123",
						}).
						Return("", errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "LastName",
					Password: "QwertyAsd123",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := zerolog.Nop()
			s := NewUsersPublicService(&logger, tt.fields.useCase())
			got, err := s.SignUp(tt.args.ctx, tt.args.req)
			if tt.wantErr {
				require.Error(t, err)
				st, ok := status.FromError(err)
				require.True(t, ok)
				require.Equal(t, tt.wantErrCode.String(), st.Code().String())
			} else {
				require.Nil(t, err)
				require.NotNil(t, got)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func TestUsersPublicService_SignIn(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		useCase func() usersUseCase
	}
	type args struct {
		ctx context.Context
		req *userspb.SignInRequest
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		want        *userspb.SignInResponse
		wantErr     bool
		wantErrCode codes.Code
	}{
		{
			name: "success",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						SignIn(gomock.Any(), "user@example.com", "password").
						Return("access_token", "refresh_token", nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignInRequest{
					Email:    "user@example.com",
					Password: "password",
				},
			},
			want: &userspb.SignInResponse{
				AccessToken:  "access_token",
				RefreshToken: "refresh_token",
			},
		},
		{
			name:   "invalid request",
			fields: fields{useCase: func() usersUseCase { return nil }},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignInRequest{},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "user not found by email",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						SignIn(gomock.Any(), "user@example.com", "password").
						Return("", "", repository.ErrNotFound)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignInRequest{
					Email:    "user@example.com",
					Password: "password",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "invalid password",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						SignIn(gomock.Any(), "user@example.com", "password").
						Return("", "", domain.ErrInvalidPassword)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignInRequest{
					Email:    "user@example.com",
					Password: "password",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "error",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						SignIn(gomock.Any(), "user@example.com", "password").
						Return("", "", errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.SignInRequest{
					Email:    "user@example.com",
					Password: "password",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := zerolog.Nop()
			s := NewUsersPublicService(&logger, tt.fields.useCase())
			got, err := s.SignIn(tt.args.ctx, tt.args.req)
			if tt.wantErr {
				require.Error(t, err)
				st, ok := status.FromError(err)
				require.True(t, ok)
				require.Equal(t, tt.wantErrCode.String(), st.Code().String())
			} else {
				require.Nil(t, err)
				require.NotNil(t, got)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func TestUsersPublicService_RefreshTokens(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		useCase func() usersUseCase
	}
	type args struct {
		ctx context.Context
		req *userspb.RefreshTokensRequest
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		want        *userspb.RefreshTokensResponse
		wantErr     bool
		wantErrCode codes.Code
	}{
		{
			name: "success",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Refresh(gomock.Any(), "refresh_token").
						Return("new_access_token", "new_refresh_token", nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.RefreshTokensRequest{RefreshToken: "refresh_token"},
			},
			want: &userspb.RefreshTokensResponse{
				AccessToken:  "new_access_token",
				RefreshToken: "new_refresh_token",
			},
		},
		{
			name:   "invalid request",
			fields: fields{useCase: func() usersUseCase { return nil }},
			args: args{
				ctx: context.Background(),
				req: &userspb.RefreshTokensRequest{},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "user not found",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Refresh(gomock.Any(), "refresh_token").
						Return("", "", repository.ErrNotFound)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.RefreshTokensRequest{RefreshToken: "refresh_token"},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "invalid token",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Refresh(gomock.Any(), "refresh_token").
						Return("", "", domain.ErrInvalidToken)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.RefreshTokensRequest{RefreshToken: "refresh_token"},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "error",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Refresh(gomock.Any(), "refresh_token").
						Return("", "", errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.RefreshTokensRequest{RefreshToken: "refresh_token"},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := zerolog.Nop()
			s := NewUsersPublicService(&logger, tt.fields.useCase())
			got, err := s.RefreshTokens(tt.args.ctx, tt.args.req)
			if tt.wantErr {
				require.Error(t, err)
				st, ok := status.FromError(err)
				require.True(t, ok)
				require.Equal(t, tt.wantErrCode.String(), st.Code().String())
			} else {
				require.Nil(t, err)
				require.NotNil(t, got)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func TestUsersPublicService_UserInfo(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		useCase func() usersUseCase
	}
	type args struct {
		ctx context.Context
		req *userspb.UserInfoRequest
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		want        *userspb.UserInfoResponse
		wantErr     bool
		wantErrCode codes.Code
	}{
		{
			name: "success with last login",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeAccess).
						Return(domain.User{
							ID:           1,
							UUID:         "4f8fbe57-01b7-48c9-bc9d-754d8577f52c",
							FirstName:    "Name",
							LastName:     "LastName",
							Email:        "user@example.com",
							PasswordHash: "password_hash",
							CreatedAt:    time.Now().UTC().Truncate(time.Hour),
							UpdatedAt:    time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10),
							LastLoginAt:  time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15),
						}, nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoRequest{AccessToken: "token"},
			},
			want: &userspb.UserInfoResponse{Data: &userspb.UserData{
				UserUuid:  "4f8fbe57-01b7-48c9-bc9d-754d8577f52c",
				Name:      "Name",
				LastName:  "LastName",
				Email:     "user@example.com",
				CreatedAt: timestamppb.New(time.Now().UTC().Truncate(time.Hour)),
				UpdatedAt: timestamppb.New(time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10)),
				LastLogin: timestamppb.New(time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15)),
			}},
		},
		{
			name: "success without last login",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeAccess).
						Return(domain.User{
							ID:           1,
							UUID:         "4f8fbe57-01b7-48c9-bc9d-754d8577f52c",
							FirstName:    "Name",
							LastName:     "LastName",
							Email:        "user@example.com",
							PasswordHash: "password_hash",
							CreatedAt:    time.Now().UTC().Truncate(time.Hour),
							UpdatedAt:    time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10),
							LastLoginAt:  time.Time{},
						}, nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoRequest{AccessToken: "token"},
			},
			want: &userspb.UserInfoResponse{Data: &userspb.UserData{
				UserUuid:  "4f8fbe57-01b7-48c9-bc9d-754d8577f52c",
				Name:      "Name",
				LastName:  "LastName",
				Email:     "user@example.com",
				CreatedAt: timestamppb.New(time.Now().UTC().Truncate(time.Hour)),
				UpdatedAt: timestamppb.New(time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10)),
				LastLogin: nil,
			}},
		},
		{
			name: "invalid request",
			fields: fields{useCase: func() usersUseCase {
				m := mocks.NewMockusersUseCase(ctrl)
				return m
			}},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoRequest{},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "invalid token",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeAccess).
						Return(domain.User{}, domain.ErrInvalidToken)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoRequest{AccessToken: "token"},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "no user",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeAccess).
						Return(domain.User{}, repository.ErrNotFound)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoRequest{AccessToken: "token"},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "error",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeAccess).
						Return(domain.User{}, errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoRequest{AccessToken: "token"},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := zerolog.Nop()
			s := NewUsersPublicService(&logger, tt.fields.useCase())
			got, err := s.UserInfo(tt.args.ctx, tt.args.req)
			if tt.wantErr {
				require.Error(t, err)
				st, ok := status.FromError(err)
				require.True(t, ok)
				require.Equal(t, tt.wantErrCode.String(), st.Code().String())
			} else {
				require.Nil(t, err)
				require.NotNil(t, got)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func TestUsersPublicService_ForgotPassword(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		useCase func() usersUseCase
	}
	type args struct {
		ctx context.Context
		req *userspb.ForgotPasswordRequest
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantErr     bool
		wantErrCode codes.Code
	}{
		{
			name: "success",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						ForgotPassword(gomock.Any(), "user@example.com").
						Return(nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ForgotPasswordRequest{Email: "user@example.com"},
			},
		},
		{
			name: "invalid request",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ForgotPasswordRequest{},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "user not found",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						ForgotPassword(gomock.Any(), "user@example.com").
						Return(repository.ErrNotFound)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ForgotPasswordRequest{Email: "user@example.com"},
			},
			wantErr:     true,
			wantErrCode: codes.NotFound,
		},
		{
			name: "error",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						ForgotPassword(gomock.Any(), "user@example.com").
						Return(errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ForgotPasswordRequest{Email: "user@example.com"},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := zerolog.Nop()
			s := NewUsersPublicService(&logger, tt.fields.useCase())
			got, err := s.ForgotPassword(tt.args.ctx, tt.args.req)
			if tt.wantErr {
				require.Error(t, err)
				st, ok := status.FromError(err)
				require.True(t, ok)
				require.Equal(t, tt.wantErrCode.String(), st.Code().String())
			} else {
				require.Nil(t, err)
				require.NotNil(t, got)
			}
		})
	}
}

func TestUsersPublicService_ResetPassword(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		useCase func() usersUseCase
	}
	type args struct {
		ctx context.Context
		req *userspb.ResetPasswordRequest
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantErr     bool
		wantErrCode codes.Code
	}{
		{
			name: "success",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeResetPassword).
						Return(domain.User{
							ID:    1,
							UUID:  "dfc2ad81-d331-4087-9d8b-1da70ca99d43",
							Email: "user@example.com",
						}, nil)
					m.EXPECT().
						SetPassword(gomock.Any(), domain.UserID(1), "password").
						Return(nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: "password",
				},
			},
		},
		{
			name: "invalid request",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ResetPasswordRequest{},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "invalid token",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeResetPassword).
						Return(domain.User{}, domain.ErrInvalidToken)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: "password",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "no user",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeResetPassword).
						Return(domain.User{}, repository.ErrNotFound)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: "password",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Unauthenticated,
		},
		{
			name: "auth error",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeResetPassword).
						Return(domain.User{}, errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: "password",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
		{
			name: "set password error",
			fields: fields{
				useCase: func() usersUseCase {
					m := mocks.NewMockusersUseCase(ctrl)
					m.EXPECT().
						Auth(gomock.Any(), "token", domain.TokenTypeResetPassword).
						Return(domain.User{
							ID:    1,
							UUID:  "dfc2ad81-d331-4087-9d8b-1da70ca99d43",
							Email: "user@example.com",
						}, nil)
					m.EXPECT().
						SetPassword(gomock.Any(), domain.UserID(1), "password").
						Return(errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: "password",
				},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := zerolog.Nop()
			s := NewUsersPublicService(&logger, tt.fields.useCase())
			got, err := s.ResetPassword(tt.args.ctx, tt.args.req)
			if tt.wantErr {
				require.Error(t, err)
				st, ok := status.FromError(err)
				require.True(t, ok)
				require.Equal(t, tt.wantErrCode.String(), st.Code().String())
			} else {
				require.Nil(t, err)
				require.NotNil(t, got)
			}
		})
	}
}
