// Code generated by MockGen. DO NOT EDIT.
// Source: users_public.go

// Package mocks is a generated GoMock package.
package mocks

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	domain "gitlab.com/ms-example/users-service/internal/domain"
)

// MockusersUseCase is a mock of usersUseCase interface.
type MockusersUseCase struct {
	ctrl     *gomock.Controller
	recorder *MockusersUseCaseMockRecorder
}

// MockusersUseCaseMockRecorder is the mock recorder for MockusersUseCase.
type MockusersUseCaseMockRecorder struct {
	mock *MockusersUseCase
}

// NewMockusersUseCase creates a new mock instance.
func NewMockusersUseCase(ctrl *gomock.Controller) *MockusersUseCase {
	mock := &MockusersUseCase{ctrl: ctrl}
	mock.recorder = &MockusersUseCaseMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockusersUseCase) EXPECT() *MockusersUseCaseMockRecorder {
	return m.recorder
}

// Auth mocks base method.
func (m *MockusersUseCase) Auth(ctx context.Context, accessToken string, tokenType domain.TokenType) (domain.User, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Auth", ctx, accessToken, tokenType)
	ret0, _ := ret[0].(domain.User)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Auth indicates an expected call of Auth.
func (mr *MockusersUseCaseMockRecorder) Auth(ctx, accessToken, tokenType interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Auth", reflect.TypeOf((*MockusersUseCase)(nil).Auth), ctx, accessToken, tokenType)
}

// ForgotPassword mocks base method.
func (m *MockusersUseCase) ForgotPassword(ctx context.Context, email string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ForgotPassword", ctx, email)
	ret0, _ := ret[0].(error)
	return ret0
}

// ForgotPassword indicates an expected call of ForgotPassword.
func (mr *MockusersUseCaseMockRecorder) ForgotPassword(ctx, email interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ForgotPassword", reflect.TypeOf((*MockusersUseCase)(nil).ForgotPassword), ctx, email)
}

// Refresh mocks base method.
func (m *MockusersUseCase) Refresh(ctx context.Context, refreshToken string) (string, string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Refresh", ctx, refreshToken)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(string)
	ret2, _ := ret[2].(error)
	return ret0, ret1, ret2
}

// Refresh indicates an expected call of Refresh.
func (mr *MockusersUseCaseMockRecorder) Refresh(ctx, refreshToken interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Refresh", reflect.TypeOf((*MockusersUseCase)(nil).Refresh), ctx, refreshToken)
}

// SetPassword mocks base method.
func (m *MockusersUseCase) SetPassword(ctx context.Context, userID domain.UserID, password string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetPassword", ctx, userID, password)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetPassword indicates an expected call of SetPassword.
func (mr *MockusersUseCaseMockRecorder) SetPassword(ctx, userID, password interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetPassword", reflect.TypeOf((*MockusersUseCase)(nil).SetPassword), ctx, userID, password)
}

// SignIn mocks base method.
func (m *MockusersUseCase) SignIn(ctx context.Context, email, password string) (string, string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SignIn", ctx, email, password)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(string)
	ret2, _ := ret[2].(error)
	return ret0, ret1, ret2
}

// SignIn indicates an expected call of SignIn.
func (mr *MockusersUseCaseMockRecorder) SignIn(ctx, email, password interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignIn", reflect.TypeOf((*MockusersUseCase)(nil).SignIn), ctx, email, password)
}

// SignUp mocks base method.
func (m *MockusersUseCase) SignUp(ctx context.Context, req *domain.SignUpRequest) (string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SignUp", ctx, req)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// SignUp indicates an expected call of SignUp.
func (mr *MockusersUseCaseMockRecorder) SignUp(ctx, req interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SignUp", reflect.TypeOf((*MockusersUseCase)(nil).SignUp), ctx, req)
}
