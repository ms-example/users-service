package grpc

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"gitlab.com/ms-example/protobuf/userspb"
)

func validateSignUpReq(req *userspb.SignUpRequest) error {
	return validation.ValidateStruct(req,
		validation.Field(&req.Name, validation.Required, validation.Length(1, 50)),
		validation.Field(&req.LastName, validation.Required, validation.Length(1, 50)),
		validation.Field(&req.Email, validation.Required, is.EmailFormat, validation.Length(3, 320)),
		validation.Field(&req.Password, validation.Required, validation.Length(8, 50), is.Alphanumeric),
	)
}

func validateSignInReq(req *userspb.SignInRequest) error {
	return validation.ValidateStruct(req,
		validation.Field(&req.Email, validation.Required),
		validation.Field(&req.Password, validation.Required),
	)
}

func validateRefreshTokensReq(req *userspb.RefreshTokensRequest) error {
	return validation.ValidateStruct(req,
		validation.Field(&req.RefreshToken, validation.Required),
	)
}

func validateUserInfoReq(req *userspb.UserInfoRequest) error {
	return validation.ValidateStruct(req,
		validation.Field(&req.AccessToken, validation.Required),
	)
}

func validateUserInfoInternalReq(req *userspb.UserInfoInternalRequest) error {
	return validation.ValidateStruct(req,
		validation.Field(&req.Id, validation.Required),
	)
}

func validateForgotPasswordReq(req *userspb.ForgotPasswordRequest) error {
	return validation.ValidateStruct(req,
		validation.Field(&req.Email, validation.Required),
	)
}

func validateResetPasswordReq(req *userspb.ResetPasswordRequest) error {
	return validation.ValidateStruct(req,
		validation.Field(&req.Token, validation.Required),
		validation.Field(&req.Password, validation.Required, validation.Length(8, 50), is.Alphanumeric),
	)
}
