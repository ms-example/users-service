package grpc

import (
	"context"
	"testing"
	"time"

	"gitlab.com/ms-example/users-service/internal/api/grpc/mocks"
	"gitlab.com/ms-example/users-service/internal/domain"
	"gitlab.com/ms-example/users-service/internal/repository"

	"github.com/golang/mock/gomock"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"gitlab.com/ms-example/protobuf/userspb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestUsersInternalService_UserInfo(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Cleanup(ctrl.Finish)

	type fields struct {
		useCase func() usersInternalUseCase
	}
	type args struct {
		ctx context.Context
		req *userspb.UserInfoInternalRequest
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		want        *userspb.UserInfoInternalResponse
		wantErr     bool
		wantErrCode codes.Code
	}{
		{
			name: "success",
			fields: fields{
				useCase: func() usersInternalUseCase {
					m := mocks.NewMockusersInternalUseCase(ctrl)
					m.EXPECT().
						GetByID(gomock.Any(), domain.UserID(1)).
						Return(domain.User{
							ID:           1,
							UUID:         "124feb91-a289-48a6-befd-b04f5341a407",
							FirstName:    "Name",
							LastName:     "LastName",
							Email:        "user@example.com",
							PasswordHash: "password_hash",
							CreatedAt:    time.Now().UTC().Truncate(time.Hour),
							UpdatedAt:    time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10),
							LastLoginAt:  time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15),
						}, nil)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoInternalRequest{Id: 1},
			},
			want: &userspb.UserInfoInternalResponse{
				Data: &userspb.UserData{
					UserUuid:  "124feb91-a289-48a6-befd-b04f5341a407",
					Name:      "Name",
					LastName:  "LastName",
					Email:     "user@example.com",
					CreatedAt: timestamppb.New(time.Now().UTC().Truncate(time.Hour)),
					UpdatedAt: timestamppb.New(time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 10)),
					LastLogin: timestamppb.New(time.Now().UTC().Truncate(time.Hour).Add(time.Minute * 15)),
				},
			},
		},
		{
			name: "invalid request",
			fields: fields{
				useCase: func() usersInternalUseCase {
					m := mocks.NewMockusersInternalUseCase(ctrl)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoInternalRequest{},
			},
			wantErr:     true,
			wantErrCode: codes.InvalidArgument,
		},
		{
			name: "user not found",
			fields: fields{
				useCase: func() usersInternalUseCase {
					m := mocks.NewMockusersInternalUseCase(ctrl)
					m.EXPECT().
						GetByID(gomock.Any(), domain.UserID(1)).
						Return(domain.User{}, repository.ErrNotFound)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoInternalRequest{Id: 1},
			},
			wantErr:     true,
			wantErrCode: codes.NotFound,
		},
		{
			name: "error",
			fields: fields{
				useCase: func() usersInternalUseCase {
					m := mocks.NewMockusersInternalUseCase(ctrl)
					m.EXPECT().
						GetByID(gomock.Any(), domain.UserID(1)).
						Return(domain.User{}, errTest)
					return m
				},
			},
			args: args{
				ctx: context.Background(),
				req: &userspb.UserInfoInternalRequest{Id: 1},
			},
			wantErr:     true,
			wantErrCode: codes.Internal,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logger := zerolog.Nop()
			s := NewUsersInternalService(&logger, tt.fields.useCase())
			got, err := s.UserInfo(tt.args.ctx, tt.args.req)
			if tt.wantErr {
				require.Error(t, err)
				st, ok := status.FromError(err)
				require.True(t, ok)
				require.Equal(t, tt.wantErrCode.String(), st.Code().String())
			} else {
				require.Nil(t, err)
				require.NotNil(t, got)
				require.Equal(t, tt.want, got)
			}
		})
	}
}
