package grpc

import (
	"errors"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	errGRPCInternalError = status.Error(codes.Internal, "internal error")
	errWrongCredentials  = errors.New("wrong email or password")
)

func invalidArgumentError(err error) error {
	return status.Error(codes.InvalidArgument, err.Error())
}

func unauthenticatedError(err error) error {
	return status.Error(codes.Unauthenticated, err.Error())
}

func notFoundError(err error) error {
	return status.Error(codes.NotFound, err.Error())
}
