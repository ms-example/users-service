package grpc

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ms-example/protobuf/userspb"
)

func Test_validateSignUpReq(t *testing.T) {
	type args struct {
		req *userspb.SignUpRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr string
	}{
		{
			name: "valid",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "LastName",
					Password: "qwerty1234567890",
				},
			},
		},
		{
			name: "empty name",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "",
					LastName: "LastName",
					Password: "qwerty1234567890",
				},
			},
			wantErr: "name: cannot be blank.",
		},
		{
			name: "name too long",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "123456789012345678901234567890123456789012345678901", // 51 chars
					LastName: "LastName",
					Password: "qwerty1234567890",
				},
			},
			wantErr: "name: the length must be between 1 and 50.",
		},
		{
			name: "empty last name",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "",
					Password: "qwerty1234567890",
				},
			},
			wantErr: "last_name: cannot be blank.",
		},
		{
			name: "last name too long",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "123456789012345678901234567890123456789012345678901", // 51 chars
					Password: "qwerty1234567890",
				},
			},
			wantErr: "last_name: the length must be between 1 and 50.",
		},
		{
			name: "empty email",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "",
					Name:     "Name",
					LastName: "LastName",
					Password: "qwerty1234567890",
				},
			},
			wantErr: "email: cannot be blank.",
		},
		{
			name: "wrong email format",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "email",
					Name:     "Name",
					LastName: "LastName",
					Password: "qwerty1234567890",
				},
			},
			wantErr: "email: must be a valid email address.",
		},
		{
			name: "empty password",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "LastName",
					Password: "",
				},
			},
			wantErr: "password: cannot be blank.",
		},
		{
			name: "too short password",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "LastName",
					Password: "1234567",
				},
			},
			wantErr: "password: the length must be between 8 and 50.",
		},
		{
			name: "too long password",
			args: args{
				req: &userspb.SignUpRequest{
					Email:    "user@example.com",
					Name:     "Name",
					LastName: "LastName",
					Password: "123456789012345678901234567890123456789012345678901", // 51 chars
				},
			},
			wantErr: "password: the length must be between 8 and 50.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateSignUpReq(tt.args.req)
			if tt.wantErr != "" {
				require.Error(t, err)
				require.Equal(t, tt.wantErr, err.Error())
			} else {
				require.Nil(t, err)
			}
		})
	}
}

func Test_validateSignInReq(t *testing.T) {
	type args struct {
		req *userspb.SignInRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr string
	}{
		{
			name: "valid",
			args: args{
				req: &userspb.SignInRequest{
					Email:    "user@example.com",
					Password: "qwerty123456",
				},
			},
		},
		{
			name: "empty email",
			args: args{
				req: &userspb.SignInRequest{
					Email:    "",
					Password: "qwerty123456",
				},
			},
			wantErr: "email: cannot be blank.",
		},
		{
			name: "empty password",
			args: args{
				req: &userspb.SignInRequest{
					Email:    "user@example.com",
					Password: "",
				},
			},
			wantErr: "password: cannot be blank.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateSignInReq(tt.args.req)
			if tt.wantErr != "" {
				require.Error(t, err)
				require.Equal(t, tt.wantErr, err.Error())
			} else {
				require.Nil(t, err)
			}
		})
	}
}

func Test_validateRefreshTokensReq(t *testing.T) {
	type args struct {
		req *userspb.RefreshTokensRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr string
	}{
		{
			name: "success",
			args: args{req: &userspb.RefreshTokensRequest{RefreshToken: "refresh_token"}},
		},
		{
			name:    "empty refresh token",
			args:    args{req: &userspb.RefreshTokensRequest{RefreshToken: ""}},
			wantErr: "refresh_token: cannot be blank.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateRefreshTokensReq(tt.args.req)
			if tt.wantErr != "" {
				require.Error(t, err)
				require.Equal(t, tt.wantErr, err.Error())
			} else {
				require.Nil(t, err)
			}
		})
	}
}

func Test_validateUserInfoReq(t *testing.T) {
	type args struct {
		req *userspb.UserInfoRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr string
	}{
		{
			name: "success",
			args: args{req: &userspb.UserInfoRequest{AccessToken: "token"}},
		},
		{
			name:    "empty access token",
			args:    args{req: &userspb.UserInfoRequest{}},
			wantErr: "access_token: cannot be blank.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateUserInfoReq(tt.args.req)
			if tt.wantErr != "" {
				require.Error(t, err)
				require.Equal(t, tt.wantErr, err.Error())
			} else {
				require.Nil(t, err)
			}
		})
	}
}

func Test_validateUserInfoInternalReq(t *testing.T) {
	type args struct {
		req *userspb.UserInfoInternalRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr string
	}{
		{
			name: "success",
			args: args{req: &userspb.UserInfoInternalRequest{Id: 1}},
		},
		{
			name:    "empty id",
			args:    args{req: &userspb.UserInfoInternalRequest{}},
			wantErr: "id: cannot be blank.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateUserInfoInternalReq(tt.args.req)
			if tt.wantErr != "" {
				require.Error(t, err)
				require.Equal(t, tt.wantErr, err.Error())
			} else {
				require.Nil(t, err)
			}
		})
	}
}

func Test_validateForgotPasswordReq(t *testing.T) {
	type args struct {
		req *userspb.ForgotPasswordRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr string
	}{
		{
			name: "valid",
			args: args{req: &userspb.ForgotPasswordRequest{Email: "user@example.com"}},
		},
		{
			name:    "empty email",
			args:    args{req: &userspb.ForgotPasswordRequest{}},
			wantErr: "email: cannot be blank.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateForgotPasswordReq(tt.args.req)
			if tt.wantErr != "" {
				require.Error(t, err)
				require.Equal(t, tt.wantErr, err.Error())
			} else {
				require.Nil(t, err)
			}
		})
	}
}

func Test_validateResetPasswordReq(t *testing.T) {
	type args struct {
		req *userspb.ResetPasswordRequest
	}
	tests := []struct {
		name    string
		args    args
		wantErr string
	}{
		{
			name: "success",
			args: args{
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: "password",
				},
			},
		},
		{
			name:    "empty token",
			args:    args{req: &userspb.ResetPasswordRequest{Password: "password"}},
			wantErr: "token: cannot be blank.",
		},
		{
			name:    "empty password",
			args:    args{req: &userspb.ResetPasswordRequest{Token: "token"}},
			wantErr: "password: cannot be blank.",
		},
		{
			name: "too short password",
			args: args{
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: "123",
				},
			},
			wantErr: "password: the length must be between 8 and 50.",
		},
		{
			name: "too long password",
			args: args{
				req: &userspb.ResetPasswordRequest{
					Token:    "token",
					Password: string(make([]byte, 51)),
				},
			},
			wantErr: "password: the length must be between 8 and 50.",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validateResetPasswordReq(tt.args.req)
			if tt.wantErr != "" {
				require.Error(t, err)
				require.Equal(t, tt.wantErr, err.Error())
			} else {
				require.Nil(t, err)
			}
		})
	}
}
