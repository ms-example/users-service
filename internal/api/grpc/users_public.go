// Package grpc provides implementation of gRPC services
//
//go:generate mockgen -source=users_public.go -destination=./mocks/users_public.go -package mocks
package grpc

import (
	"context"
	"errors"

	"gitlab.com/ms-example/users-service/internal/domain"
	"gitlab.com/ms-example/users-service/internal/repository"
	"gitlab.com/ms-example/users-service/internal/tag"
	"gitlab.com/ms-example/users-service/pkg/msxtrace"

	"github.com/rs/zerolog"
	"gitlab.com/ms-example/protobuf/userspb"
	"go.opentelemetry.io/otel/attribute"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type usersUseCase interface {
	SignUp(ctx context.Context, req *domain.SignUpRequest) (string, error)
	SignIn(ctx context.Context, email, password string) (accessToken, refreshToken string, err error)
	Refresh(ctx context.Context, refreshToken string) (accessTokenNew, refreshTokenNew string, err error)
	Auth(ctx context.Context, accessToken string, tokenType domain.TokenType) (domain.User, error)
	ForgotPassword(ctx context.Context, email string) error
	SetPassword(ctx context.Context, userID domain.UserID, password string) error
}

type UsersPublicService struct {
	userspb.UnimplementedUsersPublicServiceServer

	logger  *zerolog.Logger
	useCase usersUseCase
}

func NewUsersPublicService(
	logger *zerolog.Logger,
	useCase usersUseCase,
) *UsersPublicService {
	sLogger := logger.With().
		Str(tag.Scope, "grpc").
		Str(tag.GRPCService, userspb.UsersPublicService_ServiceDesc.ServiceName).
		Logger()
	return &UsersPublicService{
		logger:  &sLogger,
		useCase: useCase,
	}
}

func (s *UsersPublicService) SignUp(
	ctx context.Context,
	req *userspb.SignUpRequest,
) (_ *userspb.SignUpResponse, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(
		attribute.String(tag.Email, req.Email),
		attribute.String(tag.FirstName, req.Name),
		attribute.String(tag.LastName, req.LastName),
	)
	defer func() { finish(err) }()

	if err := validateSignUpReq(req); err != nil {
		return nil, invalidArgumentError(err)
	}

	signUpReq := domain.SignUpRequest{
		Name:     req.Name,
		LastName: req.LastName,
		Email:    req.Email,
		Password: req.Password,
	}

	userUUID, err := s.useCase.SignUp(ctx, &signUpReq)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrEmailAlreadyInUse):
			return nil, invalidArgumentError(domain.ErrEmailAlreadyInUse)
		default:
			s.logger.Error().
				Err(err).
				Str(tag.GRPCMethod, "SignUp").
				Msg("grpc signup error")
			return nil, errGRPCInternalError
		}
	}

	return &userspb.SignUpResponse{UserUuid: userUUID}, nil
}

func (s *UsersPublicService) SignIn(
	ctx context.Context,
	req *userspb.SignInRequest,
) (_ *userspb.SignInResponse, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(
		attribute.String(tag.Email, req.Email),
	)
	defer func() { finish(err) }()

	if err := validateSignInReq(req); err != nil {
		return nil, invalidArgumentError(err)
	}

	accessToken, refreshToken, err := s.useCase.SignIn(ctx, req.Email, req.Password)
	if err != nil {
		switch {
		case errors.Is(err, repository.ErrNotFound) || errors.Is(err, domain.ErrInvalidPassword):
			return nil, unauthenticatedError(errWrongCredentials)
		default:
			s.logger.Error().
				Err(err).
				Str(tag.GRPCMethod, "SignIn").
				Msg("grpc signin error")
			return nil, errGRPCInternalError
		}
	}

	return &userspb.SignInResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}

func (s *UsersPublicService) RefreshTokens(
	ctx context.Context,
	req *userspb.RefreshTokensRequest,
) (_ *userspb.RefreshTokensResponse, err error) {
	ctx, _, finish := msxtrace.Trace(ctx, "")
	defer func() { finish(err) }()

	if err := validateRefreshTokensReq(req); err != nil {
		return nil, invalidArgumentError(err)
	}

	accessToken, refreshToken, err := s.useCase.Refresh(ctx, req.RefreshToken)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrInvalidToken) || errors.Is(err, repository.ErrNotFound):
			return nil, unauthenticatedError(err)
		default:
			s.logger.Error().
				Err(err).
				Str(tag.GRPCMethod, "RefreshTokens").
				Msg("grpc refresh tokens error")
			return nil, errGRPCInternalError
		}
	}

	return &userspb.RefreshTokensResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}

func (s *UsersPublicService) UserInfo(
	ctx context.Context,
	req *userspb.UserInfoRequest,
) (_ *userspb.UserInfoResponse, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	defer func() { finish(err) }()

	if err := validateUserInfoReq(req); err != nil {
		return nil, invalidArgumentError(err)
	}

	user, err := s.useCase.Auth(ctx, req.AccessToken, domain.TokenTypeAccess)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrInvalidToken) || errors.Is(err, repository.ErrNotFound):
			return nil, unauthenticatedError(err)
		default:
			s.logger.Error().
				Err(err).
				Str(tag.GRPCMethod, "UserInfo").
				Msg("grpc auth error")
			return nil, errGRPCInternalError
		}
	}

	span.SetAttributes(
		attribute.Int(tag.UserID, user.ID.Int()),
		attribute.String(tag.UserUUID, user.UUID),
	)

	return &userspb.UserInfoResponse{Data: userToProto(&user)}, nil
}

func (s *UsersPublicService) ForgotPassword(
	ctx context.Context,
	req *userspb.ForgotPasswordRequest,
) (_ *emptypb.Empty, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(attribute.String(tag.Email, req.Email))
	defer func() { finish(err) }()

	if err := validateForgotPasswordReq(req); err != nil {
		return nil, invalidArgumentError(err)
	}

	err = s.useCase.ForgotPassword(ctx, req.Email)
	if err != nil {
		switch {
		case errors.Is(err, repository.ErrNotFound):
			return nil, notFoundError(repository.ErrNotFound)
		default:
			s.logger.Error().
				Err(err).
				Str(tag.GRPCMethod, "ForgotPassword").
				Msg("grpc forgot password error")
			return nil, errGRPCInternalError
		}
	}

	return &emptypb.Empty{}, nil
}

func (s *UsersPublicService) ResetPassword(
	ctx context.Context,
	req *userspb.ResetPasswordRequest,
) (_ *emptypb.Empty, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	defer func() { finish(err) }()

	if err := validateResetPasswordReq(req); err != nil {
		return nil, invalidArgumentError(err)
	}

	user, err := s.useCase.Auth(ctx, req.Token, domain.TokenTypeResetPassword)
	if err != nil {
		switch {
		case errors.Is(err, domain.ErrInvalidToken) || errors.Is(err, repository.ErrNotFound):
			return nil, unauthenticatedError(err)
		default:
			s.logger.Error().
				Err(err).
				Str(tag.GRPCMethod, "ResetPassword").
				Msg("auth error")
			return nil, errGRPCInternalError
		}
	}

	span.SetAttributes(
		attribute.Int(tag.UserID, user.ID.Int()),
		attribute.String(tag.UserUUID, user.UUID),
	)

	err = s.useCase.SetPassword(ctx, user.ID, req.Password)
	if err != nil {
		s.logger.Error().
			Err(err).
			Str(tag.GRPCMethod, "ResetPassword").
			Msg("set password error")
		return nil, errGRPCInternalError
	}

	return &emptypb.Empty{}, nil
}

func userToProto(user *domain.User) *userspb.UserData {
	var lastLoginAt *timestamppb.Timestamp
	if !user.LastLoginAt.IsZero() {
		lastLoginAt = timestamppb.New(user.LastLoginAt)
	}

	return &userspb.UserData{
		UserUuid:  user.UUID,
		Name:      user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		CreatedAt: timestamppb.New(user.CreatedAt),
		UpdatedAt: timestamppb.New(user.UpdatedAt),
		LastLogin: lastLoginAt,
	}
}
