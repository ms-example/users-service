//go:generate mockgen -source=users_internal.go -destination=./mocks/users_internal.go -package mocks
package grpc

import (
	"context"
	"errors"

	"gitlab.com/ms-example/users-service/internal/domain"
	"gitlab.com/ms-example/users-service/internal/repository"
	"gitlab.com/ms-example/users-service/internal/tag"
	"gitlab.com/ms-example/users-service/pkg/msxtrace"

	"github.com/rs/zerolog"
	"gitlab.com/ms-example/protobuf/userspb"
	"go.opentelemetry.io/otel/attribute"
)

type usersInternalUseCase interface {
	GetByID(ctx context.Context, id domain.UserID) (domain.User, error)
}

type UsersInternalService struct {
	userspb.UnimplementedUsersInternalServiceServer

	logger  *zerolog.Logger
	useCase usersInternalUseCase
}

func NewUsersInternalService(
	logger *zerolog.Logger,
	useCase usersInternalUseCase,
) *UsersInternalService {
	sLogger := logger.With().
		Str(tag.Scope, "grpc").
		Str(tag.GRPCService, userspb.UsersInternalService_ServiceDesc.ServiceName).
		Logger()
	return &UsersInternalService{
		logger:  &sLogger,
		useCase: useCase,
	}
}

func (s *UsersInternalService) UserInfo(
	ctx context.Context,
	req *userspb.UserInfoInternalRequest,
) (_ *userspb.UserInfoInternalResponse, err error) {
	ctx, span, finish := msxtrace.Trace(ctx, "")
	span.SetAttributes(
		attribute.Int(tag.UserID, int(req.Id)),
	)
	defer func() { finish(err) }()

	if err := validateUserInfoInternalReq(req); err != nil {
		return nil, invalidArgumentError(err)
	}

	user, err := s.useCase.GetByID(ctx, domain.UserID(req.Id))
	if err != nil {
		switch {
		case errors.Is(err, repository.ErrNotFound):
			return nil, notFoundError(repository.ErrNotFound)
		default:
			s.logger.Error().
				Err(err).
				Str(tag.GRPCMethod, "UserInfo").
				Msg("grpc user info error")
			return nil, errGRPCInternalError
		}
	}

	return &userspb.UserInfoInternalResponse{Data: userToProto(&user)}, nil
}
