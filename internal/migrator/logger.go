// Package migrator provides functionality for applying database migrations
package migrator

import (
	"fmt"
	"strings"

	"gitlab.com/ms-example/users-service/internal/tag"

	"github.com/pressly/goose/v3"
	"github.com/rs/zerolog"
)

var _ goose.Logger = (*Logger)(nil)

type Logger struct {
	logger zerolog.Logger
}

func NewLogger(zLogger *zerolog.Logger, db, method string) *Logger {
	loggerWithFields := zLogger.With().
		Str(tag.Scope, "migrator:"+db).
		Str(tag.Method, method).
		Logger()
	return &Logger{logger: loggerWithFields}
}

func (l *Logger) Print(v ...any) {
	l.logger.Info().Msg(fmt.Sprint(v...))
}

func (l *Logger) Println(v ...any) {
	l.logger.Info().Msg(fmt.Sprint(v...))
}

func (l *Logger) Printf(format string, v ...any) {
	format = strings.Replace(format, "\n", "", 1)
	l.logger.Info().Msg(fmt.Sprintf(format, v...))
}

func (l *Logger) Fatal(v ...any) {
	l.logger.Error().Msg(fmt.Sprint(v...))
	// os.Exit(1)
}

func (l *Logger) Fatalf(format string, v ...any) {
	format = strings.Replace(format, "\n", "", 1)
	l.logger.Error().Msg(fmt.Sprintf(format, v...))
	// os.Exit(1)
}
