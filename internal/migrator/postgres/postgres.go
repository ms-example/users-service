// Package postgres is a migrator for PostgreSQL database
package postgres

import (
	"database/sql"
	"fmt"

	"gitlab.com/ms-example/users-service/internal/migrator"

	"github.com/pressly/goose/v3"
	"github.com/rs/zerolog"

	_ "github.com/lib/pq"                                                   // init driver
	_ "gitlab.com/ms-example/users-service/deployments/migrations/postgres" // init go migrations
)

func Up(connString, migrationsPath string, logger *zerolog.Logger) error {
	goose.SetLogger(migrator.NewLogger(logger, "postgres", "Up"))

	db, err := connect(connString)
	if err != nil {
		return err
	}

	defer func() { _ = db.Close() }()

	return goose.Up(db, migrationsPath)
}

func connect(connString string) (*sql.DB, error) {
	db, err := goose.OpenDBWithDriver("postgres", connString)
	if err != nil {
		return nil, fmt.Errorf("postgres: open db: %w", err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("postgres: ping: %w", err)
	}

	return db, nil
}
