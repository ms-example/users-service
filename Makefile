NAME           ?= users-service
VOLUME_PROJECT ?= $(PWD)
VOLUME_GOPATH  ?= ${shell go env GOPATH}
CONTAINER_NAME ?= ${NAME}_cli
GO_IMAGE_NAME  ?= ${NAME}:cli
CONTAINER_RUN  ?= @docker run --rm --name=${CONTAINER_NAME} \
                  -v ${VOLUME_PROJECT}:/go/src/service \
                  -v ${VOLUME_GOPATH}/pkg:/go/pkg \
                  ${GO_IMAGE_NAME}

.DEFAULT_GOAL := help

LINTER := $(GOPATH)/bin/golangci-lint

$(LINTER):
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.52

GOOSE := $(GOPATH)/bin/goose

$(GOOSE):
	@go install github.com/pressly/goose/v3/cmd/goose@latest

GRPCUI := $(GOPATH)/bin/grpcui

$(GRPCUI):
	@go install github.com/fullstorydev/grpcui/cmd/grpcui@latest

GRPCURL := $(GOPATH)/bin/grpcurl

$(GRPCURL):
	@go install github.com/fullstorydev/grpcurl/cmd/grpcurl@latest

.PHONY: help
help: ## Prints this message.
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

.PHONY: clean
clean: ## Cleans the project from unnecessary files.
	@rm cover.out

.PHONY: lint
lint: | $(LINTER) ## Runs linter.
	@golangci-lint run -v

.PHONY: test
test: ## Runs unit tests.
	@go test -short -race -timeout 10s -count 1 -coverprofile=cover.out ./...

.PHONY: test.integration
test.integration: ## Runs integration tests.
	@go test --tags=integration -v -race -timeout 10s -count 1 -coverprofile=cover.out ./...; true

.PHONY: cover
cover: ## Specifies the percentage of code coverage by tests.
	@go tool cover -func=cover.out

.PHONY: cover.html
cover.html: ## Specifies the percentage of code coverage by tests.
	@go tool cover -html=cover.out

.PHONY: go.image.build
go.image.build: ## Builds console go container.
	@echo "-> build go console"
	@docker build -f ./console/Dockerfile ./console -t ${GO_IMAGE_NAME}

.PHONY: generate
generate: go.image.build ## Runs go generate.
	@echo "-> run go generate"
	${CONTAINER_RUN} go generate ./...

.PHONY: tools
tools: | $(GOOSE) $(LINTER) $(GRPCUI) $(GRPCURL) ## Installs external tools.
	@echo "-> tools installed"

.PHONY: grpcui
grpcui: | $(GRPCUI) ## Runs grpcui server for gRPC server on port 8090.
	grpcui -plaintext localhost:8090
