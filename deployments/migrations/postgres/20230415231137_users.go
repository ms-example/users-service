// Package postgres contains migrations for PostgreSQL database
package postgres

import (
	"database/sql"

	"github.com/pressly/goose/v3"
)

func init() {
	goose.AddMigration(upUsers, downUsers)
}

func upUsers(tx *sql.Tx) error {
	// language=postgresql
	const createTable = `
CREATE TABLE "users"
(
    id BIGSERIAL NOT NULL,
    uuid UUID NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(320) NOT NULL,
    password_hash TEXT NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
	last_login_at TIMESTAMP WITHOUT TIME ZONE NULL,
    CONSTRAINT users_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);
`
	if _, err := tx.Exec(createTable); err != nil {
		return err
	}

	// language=postgresql
	const uuidIdx = `CREATE UNIQUE INDEX idx_users_uuid_uniq ON "users" USING btree (uuid);`
	if _, err := tx.Exec(uuidIdx); err != nil {
		return err
	}

	// language=postgresql
	const emailIdx = `CREATE UNIQUE INDEX idx_users_email_uniq ON "users" USING btree (email);`
	if _, err := tx.Exec(emailIdx); err != nil {
		return err
	}

	return nil
}

func downUsers(tx *sql.Tx) error {
	// language=postgresql
	const dropUUIDIdx = `DROP INDEX idx_users_uuid_uniq;`
	if _, err := tx.Exec(dropUUIDIdx); err != nil {
		return err
	}

	// language=postgresql
	const dropEmailIdx = `DROP INDEX idx_users_email_uniq;`
	if _, err := tx.Exec(dropEmailIdx); err != nil {
		return err
	}

	// language=postgresql
	const dropTable = `DROP TABLE "users";`
	if _, err := tx.Exec(dropTable); err != nil {
		return err
	}

	return nil
}
