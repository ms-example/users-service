package main

import (
	"fmt"
	"net"
	"net/http"
	"net/http/pprof"

	"gitlab.com/ms-example/users-service/pkg/httpserver"

	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func newTechServer(lis net.Listener) *httpserver.Server {
	router := httprouter.New()

	router.HandlerFunc(http.MethodGet, "/health", func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.WriteHeader(http.StatusOK)
		_, _ = fmt.Fprintln(w, "OK")
	})

	router.Handler(http.MethodGet, "/metrics", promhttp.Handler())

	router.HandlerFunc(http.MethodGet, "/debug/pprof", pprof.Index)
	router.HandlerFunc(http.MethodGet, "/debug/cmdline", pprof.Cmdline)
	router.HandlerFunc(http.MethodGet, "/debug/profile", pprof.Profile)
	router.HandlerFunc(http.MethodGet, "/debug/symbol", pprof.Symbol)
	router.HandlerFunc(http.MethodGet, "/debug/trace", pprof.Trace)
	router.Handler(http.MethodGet, "/debug/allocs", pprof.Handler("allocs"))
	router.Handler(http.MethodGet, "/debug/block", pprof.Handler("block"))
	router.Handler(http.MethodGet, "/debug/goroutine", pprof.Handler("goroutine"))
	router.Handler(http.MethodGet, "/debug/heap", pprof.Handler("heap"))
	router.Handler(http.MethodGet, "/debug/mutex", pprof.Handler("mutex"))
	router.Handler(http.MethodGet, "/debug/threadcreate", pprof.Handler("threadcreate"))

	return &httpserver.Server{
		Listener: lis,
		Handler:  router,
	}
}
