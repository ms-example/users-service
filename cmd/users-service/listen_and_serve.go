package main

import (
	"context"

	"golang.org/x/sync/errgroup"
)

type Serverer interface {
	ListenAndServe(context.Context) error
}

func ListenAndServe(ctx context.Context, servers ...Serverer) error {
	group, groupCtx := errgroup.WithContext(ctx)

	for _, server := range servers {
		srv := server
		group.Go(func() error {
			return srv.ListenAndServe(groupCtx)
		})
	}

	return group.Wait()
}
