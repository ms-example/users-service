package main

import (
	"fmt"
	"html/template"

	"gitlab.com/ms-example/users-service/internal/config"
)

type templatesRegistry struct {
	ResetPasswordEmail *template.Template
}

func initTemplates(path string) (*templatesRegistry, error) {
	tmpl, err := template.ParseGlob(path)
	if err != nil {
		return nil, fmt.Errorf("parse: %v", err)
	}

	reg := &templatesRegistry{}

	reg.ResetPasswordEmail = tmpl.Lookup(config.ResetPasswordEmailTemplateName)
	if reg.ResetPasswordEmail == nil {
		return nil, fmt.Errorf("lookup %q: not found", config.ResetPasswordEmailTemplateName)
	}

	return reg, nil
}
