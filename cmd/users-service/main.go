package main

import (
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	grpcInternal "gitlab.com/ms-example/users-service/internal/api/grpc"
	"gitlab.com/ms-example/users-service/internal/config"
	"gitlab.com/ms-example/users-service/internal/migrator/postgres"
	usersRepository "gitlab.com/ms-example/users-service/internal/repository/postgres/users"
	"gitlab.com/ms-example/users-service/internal/services/notifier"
	"gitlab.com/ms-example/users-service/internal/services/tokenizer"
	"gitlab.com/ms-example/users-service/internal/usecases/users"
	"gitlab.com/ms-example/users-service/pkg/grpcserver"
	"gitlab.com/ms-example/users-service/pkg/kafka"
	"gitlab.com/ms-example/users-service/pkg/msxtrace"

	"github.com/Shopify/sarama"
	grpcPrometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/rs/zerolog"
	"gitlab.com/ms-example/protobuf/userspb"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel"
	jaegerExporter "go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	traceSDK "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.18.0"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	serviceName           = "users-service"
	loggerTimestampFormat = "2006-01-02 15:04:05"
)

var (
	commit = "unknown"
	branch = "unknown"
)

func main() {
	console := consoleLogger()
	if err := run(console); err != nil {
		console.Error().Msg(err.Error())
		os.Exit(1)
	}
}

//nolint:revive // it's ok for this function
func run(console *zerolog.Logger) error {
	console.Info().
		Str("commit", commit).
		Str("branch", branch).
		Msg("run...")

	// init config
	console.Info().Msg("init config")
	cfg, err := config.New()
	if err != nil {
		return fmt.Errorf("parse config: %v", err)
	}

	// listen terminate signals
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	defer cancel()

	// init kafka producer
	console.Info().Msg("init Kafka producer")
	kafkaProducer, err := newKafkaProducer(cfg)
	if err != nil {
		return fmt.Errorf("init kafka async producer: %v", err)
	}
	defer func() { _ = kafkaProducer.Close() }()

	// init logger
	console.Info().Msg("init logger")
	logger := newLogger(
		cfg.LogLevel,
		kafka.NewTopicWriter(kafkaProducer, cfg.Kafka.LogsTopic, true, console),
	)

	// init tracer
	console.Info().Msg("init tracer")
	tracerProvider, err := newTracerProvider(cfg.Jaeger.Endpoint)
	if err != nil {
		return fmt.Errorf("init tracer: %v", err)
	}
	defer func() { _ = tracerProvider.Shutdown(context.Background()) }()

	// set global tracer provider
	otel.SetTracerProvider(tracerProvider)

	// up postgres migrations
	console.Info().Msg("up postgres migrations")
	err = postgres.Up(cfg.Postgres.Dial(), cfg.Postgres.MigrationsPath, logger)
	if err != nil {
		return fmt.Errorf("up postgres migrations: %v", err)
	}

	// connect to postgres
	console.Info().Msg("connect to postgres")
	pgConnPool, err := newPostgresConnPool(ctx, &cfg.Postgres)
	if err != nil {
		return fmt.Errorf("connect to postgres: %v", err)
	}

	defer pgConnPool.Close()

	// init services and controllers
	tokensService := tokenizer.New(
		[]byte(cfg.Auth.TokensSecret),
		cfg.Auth.AccessTokenTTL,
		cfg.Auth.RefreshTokenTTL,
		cfg.Auth.ResetPasswordTokenTTL,
	)

	usersRepo := usersRepository.New(pgConnPool)

	grpcPrometheus.EnableHandlingTimeHistogram()
	grpcPrometheus.EnableClientHandlingTimeHistogram()

	// gRPC client for notifier
	notifierConn, err := connectToGRPCService(ctx, cfg.NotifierGRPCAddr, tracerProvider)
	if err != nil {
		return fmt.Errorf("connect to notifier: %v", err)
	}

	defer func() { _ = notifierConn.Close() }()

	notifierService := notifier.New(notifierConn)

	templatesHolder, err := initTemplates(cfg.TemplatesPath)
	if err != nil {
		return fmt.Errorf("init templates: %v", err)
	}

	usersUseCase := users.New(
		usersRepo,
		tokensService,
		notifierService,
		templatesHolder.ResetPasswordEmail,
		cfg.ResetPasswordURI(),
	)

	// init tech server
	console.Info().Msg("listen tech port")
	techLis, err := net.Listen("tcp", net.JoinHostPort("", cfg.TechPort))
	if err != nil {
		return fmt.Errorf("listen port %s: %v", cfg.TechPort, err)
	}

	techServer := newTechServer(techLis)

	// init gRPC public server
	console.Info().Msg("listen grpc public port")
	grpcLisPublic, err := net.Listen("tcp", net.JoinHostPort("", cfg.GRPCPort))
	if err != nil {
		return fmt.Errorf("listen port %s: %v", cfg.GRPCPort, err)
	}
	defer func() { _ = grpcLisPublic.Close() }()

	grpcServerPublic := grpcserver.NewServer(grpcLisPublic, grpc.ChainUnaryInterceptor(
		grpcPrometheus.UnaryServerInterceptor,
		otelgrpc.UnaryServerInterceptor(otelgrpc.WithTracerProvider(tracerProvider)),
		grpcserver.Recovery(codes.Internal, logger),
	))
	// register implementations of services
	usersPublicImpl := grpcInternal.NewUsersPublicService(logger, usersUseCase)
	grpcServerPublic.RegisterService(&userspb.UsersPublicService_ServiceDesc, usersPublicImpl)
	grpcServerPublic.RegisterReflection()

	// init gRPC internal server
	console.Info().Msg("listen grpc internal port")
	grpcLisInternal, err := net.Listen("tcp", net.JoinHostPort("", cfg.GRPCPortInternal))
	if err != nil {
		return fmt.Errorf("listen port %s: %v", cfg.GRPCPortInternal, err)
	}
	defer func() { _ = grpcLisInternal.Close() }()

	grpcServerInternal := grpcserver.NewServer(grpcLisInternal, grpc.ChainUnaryInterceptor(
		grpcPrometheus.UnaryServerInterceptor,
		otelgrpc.UnaryServerInterceptor(otelgrpc.WithTracerProvider(tracerProvider)),
		grpcserver.Recovery(codes.Internal, logger),
	))
	usersInternalImpl := grpcInternal.NewUsersInternalService(logger, usersUseCase)
	grpcServerInternal.RegisterService(&userspb.UsersInternalService_ServiceDesc, usersInternalImpl)
	grpcServerInternal.RegisterReflection()

	console.Info().Msg("serving...")
	return ListenAndServe(ctx, techServer, grpcServerPublic, grpcServerInternal)
}

func consoleLogger() *zerolog.Logger {
	consoleWriter := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: loggerTimestampFormat}
	logger := zerolog.New(consoleWriter).
		With().
		Timestamp().
		Logger()
	return &logger
}

func newLogger(level zerolog.Level, writers ...io.Writer) *zerolog.Logger {
	zerolog.TimeFieldFormat = loggerTimestampFormat
	consoleWriter := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: loggerTimestampFormat}
	writersWithConsole := make([]io.Writer, len(writers), len(writers)+1)
	copy(writersWithConsole, writers)
	writersWithConsole = append(writersWithConsole, consoleWriter)
	hostname, _ := os.Hostname()
	logger := zerolog.New(zerolog.MultiLevelWriter(writersWithConsole...)).
		Level(level).
		With().
		Timestamp().
		Str("service", serviceName).
		Str("host", hostname).
		Logger()
	return &logger
}

func newKafkaProducer(cfg *config.Config) (sarama.AsyncProducer, error) {
	producerConfig := sarama.NewConfig()
	producerConfig.Version = sarama.MaxVersion
	producerConfig.Producer.RequiredAcks = sarama.WaitForLocal
	producerConfig.Producer.Return.Errors = true
	producerConfig.Producer.Return.Successes = false
	return sarama.NewAsyncProducer(cfg.Kafka.Addrs, producerConfig)
}

func newTracerProvider(jaegerEndpoint string) (*traceSDK.TracerProvider, error) {
	// Create the Jaeger exporter
	exp, err := jaegerExporter.New(jaegerExporter.WithCollectorEndpoint(jaegerExporter.WithEndpoint(jaegerEndpoint)))
	if err != nil {
		return nil, err
	}

	tracer := traceSDK.NewTracerProvider(
		// Always be sure to batch in production.
		traceSDK.WithBatcher(exp),
		// Record information about this application in a Resource.
		traceSDK.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(serviceName),
		)),
	)

	return tracer, nil
}

func newPostgresConnPool(ctx context.Context, cfg *config.Postgres) (*pgxpool.Pool, error) {
	pgCfg, err := pgxpool.ParseConfig(cfg.DialWithPoolSize())
	if err != nil {
		return nil, fmt.Errorf("parse config: %w", err)
	}

	pgCfg.ConnConfig.Tracer = &msxtrace.PGXTracer{}
	pgCfg.MaxConnLifetime = cfg.MaxConnLifetime
	pgCfg.MaxConnLifetimeJitter = time.Second * 5
	pgCfg.MaxConnIdleTime = cfg.MaxConnIdleTime
	pgCfg.HealthCheckPeriod = time.Second * 5

	pool, err := pgxpool.NewWithConfig(ctx, pgCfg)
	if err != nil {
		return nil, err
	}

	if err := pool.Ping(ctx); err != nil {
		return nil, fmt.Errorf("ping: %w", err)
	}

	return pool, nil
}

func connectToGRPCService(
	ctx context.Context,
	addr string,
	tracerProvider *traceSDK.TracerProvider,
) (*grpc.ClientConn, error) {
	return grpc.DialContext(
		ctx,
		addr,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(
			grpcPrometheus.UnaryClientInterceptor,
			otelgrpc.UnaryClientInterceptor(otelgrpc.WithTracerProvider(tracerProvider)),
		),
	)
}
